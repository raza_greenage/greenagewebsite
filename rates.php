
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no"/>
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title>Greenage - Provide Services | Mandi Rates</title>
  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="img/core-img/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/core-img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/core-img/favicon-16x16.png">
  <link rel="manifest" href="img/core-img/site.webmanifest">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="style.css">

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="footer, address, phone, icons" />


  <link rel="stylesheet" href="style.css">
  
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">

  <style>
    @import url('http://fonts.googleapis.com/css?family=Open+Sans:400,700');

*{
    padding:0;
    margin:0;
}

html{
    background-color: #eaf0f2;
}

body{
    font:16px/1.6 Arial;
}

footer{
    position: fixed;
    bottom: 0;
}

@media (max-height:800px){
    footer { position: static; }
    /* header { padding-top:40px; } */
}


.footer-distributed{
    background-color: #2c292f;
    box-sizing: border-box;
    width: 100%;
    text-align: left;
    font: bold 16px sans-serif;
    padding: 50px 50px 60px 50px;
    margin-top: 80px;
}

.footer-distributed .footer-left,
.footer-distributed .footer-center,
.footer-distributed .footer-right{
    display: inline-block;
    vertical-align: top;
}

/* Footer left */

.footer-distributed .footer-left{
    width: 30%;
}

.footer-distributed h3{
    color:  #ffffff;
    font: normal 36px 'Cookie', cursive;
    margin: 0;
}

/* The company logo */

.footer-distributed .footer-left img{
    width: 30%;
}

.footer-distributed h3 span{
    color:  #8cdb31;
}

/* Footer links */

.footer-distributed .footer-links{
    color:  #ffffff;
    margin: 20px 0 12px;
}

.footer-distributed .footer-links a{
    display:inline-block;
    line-height: 1.8;
    text-decoration: none;
    color:  inherit;
}

.footer-distributed .footer-company-name{
    color:  #f7f7f7;
    font-size: 14px;
    font-weight: normal;
    margin: 0;
    
}

/* Footer Center */

.footer-distributed .footer-center{
    width: 35%;
}


.footer-distributed .footer-center i{
    background-color:  #33383b;
    color: #ffffff;
    font-size: 25px;
    width: 38px;
    height: 38px;
    border-radius: 50%;
    text-align: center;
    line-height: 42px;
    margin: 10px 15px;
    vertical-align: middle;
}

.footer-distributed .footer-center i.fa-envelope{
    font-size: 17px;
    line-height: 38px;
}

.footer-distributed .footer-center p{
    display: inline-block;
    color: #ffffff;
    vertical-align: middle;
    margin:0;
}

.footer-distributed .footer-center p span{
    display:block;
    font-weight: normal;
    font-size:14px;
    line-height:2;
}

.footer-distributed .footer-center p a{
    color:  #ffffff;
    text-decoration: none;;
}


/* Footer Right */

.footer-distributed .footer-right{
    width: 30%;
}

.footer-distributed .footer-company-about{
    line-height: 20px;
    color:  #92999f;
    font-size: 13px;
    font-weight: normal;
    margin: 0;
}

.footer-distributed .footer-company-about span{
    display: block;
    color:  #ffffff;
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 20px;
}

.footer-distributed .footer-icons{
    margin-top: 25px;
}

.footer-distributed .footer-icons a{
    display: inline-block;
    width: 35px;
    height: 35px;
    cursor: pointer;
    background-color:  #33383b;
    border-radius: 2px;

    font-size: 20px;
    color: #ffffff;
    text-align: center;
    line-height: 35px;

    margin-right: 3px;
    margin-bottom: 5px;
}

/* Here is the code for Responsive Footer */
/* You can remove below code if you don't want Footer to be responsive */


@media (max-width: 880px) {

    .footer-distributed .footer-left,
    .footer-distributed .footer-center,
    .footer-distributed .footer-right{
        display: block;
        width: 100%;
        margin-bottom: 40px;
        text-align: center;
    }

    .footer-distributed .footer-center i{
        margin-left: 0;
    }

}
  </style>
</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  <!-- ##### Header Area Start ##### -->
  <header class="header-area" style="margin-bottom: 3%">
    <!-- Top Header Area -->
    <div class="top-header-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="top-header-content d-flex align-items-center justify-content-between">
              <!-- Top Header Content -->
              <div class="top-header-meta">
                <p>Welcome to <span>Greenage</span>, we hope you will enjoy our services and have good experience</p>
              </div>
              <!-- Top Header Content -->
              <div class="top-header-meta text-right">
                <a href="contact.html" data-toggle="tooltip" data-placement="bottom" title="info@greenageservices.com"><i
                    class="fa fa-envelope-o" aria-hidden="true"></i> <span>Email: info@greenageservices.com</span></a>
                <a href="contact.html" data-toggle="tooltip" data-placement="bottom" title="+92 51 846 0676"><i class="fa fa-phone"
                    aria-hidden="true"></i> <span>Call Us: +92 51 846 0676-78</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Navbar Area -->
    <div class="famie-main-menu" style="text-align: left; margin-left: 0%; margin-right: 0%;">
      <div class="classy-nav-container breakpoint-off">
        <div class="container">
          <!-- Menu -->
          <nav class="classy-navbar justify-content-between" id="famieNav">
            <!-- Nav Brand -->
            <div>
            <a href="index.html" class="nav-brand" style="margin-right: 0%; margin-left: 0%;">
             
              <img src="img/core-img/website_logo.png" alt="" width="70" height="70"></a>
              <h class="head" style="text-align: right; color: green; margin-right: 0%;">GreenAge Services</h>
             </div> 
            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
              <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>
            <!-- Menu -->
            <div class="classy-menu " style="text-align: right;">
              <!-- Close Button -->
              <div class="classycloseIcon" style="margin-left: 10%;">
                <div class="cross-wrap"><span class="top" ></span><span class="bottom"></span></div>
              </div>
              <!-- Navbar Start -->
              <div class="classynav" style="text-align: right;">
                <ul class="nav" style="text-align: right; margin-left: 0%; margin-right: 0%;">
                  <li ><a href="index.html">Home</a></li>
                  <li><a href="about.html">About</a></li>
                  <!-- <li><a href="#">Pages</a>
                    <ul class="dropdown">
                      <li><a href="index.html">Home</a></li>
                      <li><a href="about.html">About Us</a></li> -->
                      <!-- <li><a href="farming-practice.html">Farming Practice</a></li> -->
                      <!-- <li><a href="shop.html">Shop</a>
                        <ul class="dropdown">
                          <li><a href="our-product.html">Our Services</a></li> -->
                          <!-- <li><a href="shop.html">Shop</a></li> -->
                        <!-- </ul> -->
                      <!-- </li> -->
                      <!-- <li><a href="news.html">News and Events</a>
                        <ul class="dropdown"> -->
                          <!-- <li><a href="news.html">News and Events</a></li> -->
                          <!-- <li><a href="news-details.html">News Details</a></li> -->
                        <!-- </ul> -->
                      <!-- </li> -->
                      <!-- <li><a href="contact.html">Contact</a></li> -->
                    <!-- </ul> -->
                  <!-- </li> -->
                  <!-- <li><a href="our-product.html">Our Product</a></li> -->
                  <!-- <li><a href="farming-practice.html">Farming Practice</a></li> -->
                    <!-- <li><a href="#">Market Place</a></li> -->
                    <li class="active" ><a href="mandirates.html">Mandi Rates</a></li>
                    <!-- <li><a href="#">Crop Simulator</a></li> -->
                    <li><a href="contact.html">Contact</a></li>
                    <li><a href="gallery.html">Gallery</a>
                    </li>
                    <!-- <li><a href="#">Help &amp; Support</a></li> -->
                    <!-- <li><a href="#">News &amp; Events</a></li> -->
                  </ul>
                   <!-- Search Icon -->
                <div id="searchIcon">
                  <i class="icon_search" aria-hidden="true"></i>
                </div>
                  
                
                <!-- Search Icon -->
                <!-- <div id="searchIcon">
                  <i class="icon_search" aria-hidden="true"></i>
                </div> -->
                <!-- Cart Icon -->
                <!-- <div id="cartIcon">
                  <a href="#">
                    <i class="icon_cart_alt" aria-hidden="true"></i>
                    <span class="cart-quantity">2</span>
                  </a>
                </div> -->
              </div>
              <!-- Navbar End -->
            </div>
          </nav>

          <!-- Search Form -->
          <div class="search-form">
            <form action="#" method="get">
              <input type="search" name="search" id="search" placeholder="Type keywords &amp; press enter...">
              <button type="submit" class="d-none"></button>
            </form>
            <!-- Close Icon -->
            <div class="closeIcon"><i class="fa fa-times" aria-hidden="true"></i></div>
          </div>
        </div>
      </div>
    </div>
  </header>

<?php
$comodity = $_REQUEST["comodity"];
//echo $comodity;

include "simple_html_dom.php";
$websiteurl = "http://www.amis.pk/ViewPrices.aspx?searchType=1&commodityId=".$comodity;
//echo $websiteurl;
$html = file_get_html($websiteurl);
//col-9 col-s-9
$crops = array();
$rates = array();
$srno = 0;
$srmin = 7;
$srmax = 8;
$quant = 10;

foreach($html->find('.listItem') as $postDiv)
{
    foreach($postDiv->find('a') as $a)
    {
        $crops[] = $a;
    }
}

   
echo '
<div>
<table  class="listing" cellpadding="2" cellspacing="3" border="5" >
  
<tr> 
<td>Serial No.</td>  
<td>Crops</td>  
<td></td>  
<td>Min</td>  
<td></td>  
<td>Max</td>  
<td></td>  
<td>Arrival Quantity(in Quintals)</td> 
</tr>';


// echo $crops[125];

foreach($html->find('.cart') as $postDiv)
{
    foreach($postDiv->find('td') as $a)
    {
       $rates[] = $a;
    }
    for ($x = 0; $x <= sizeof($crops); $x++) {
        $srno=$srno+1;

        if(!empty($crops[$x])){

        echo '

        <tr>
        <td>'.$srno.'</td>
        <td>'.$crops[$x].'</td>
        <td>'.$rates[$srmin].'</td>
        <td>'.$rates[$srmax].'</td>
        <td>'.$rates[$quant].'</td>
        </tr>';

      }

        $srmin=$srmin+5;
        $srmax=$srmax+5;
        $quant=$quant+5;

     

    }
}

echo '</table>';

?>

<footer class="footer-distributed" style="font-family: Tahoma; margin-top: 1%;">

    <div class="footer-left" style="font-family: Tahoma;">
        <img src="img/core-img/website_logo.png" style="width:90; height:90; position: center;">
      <h3 style="font-family: Tahoma;">GreenAge<span>Services</span></h3>

    

      <p class="footer-company-name" style="font-family: Tahoma;"> Copyright © All rights reserved by GreenAge Services </p>
    </div>

    <div class="footer-center" style="font-family: Tahoma;">
      <div>
        <i class="fa fa-map-marker" style="font-family: Tahoma;">
                    </i>
          <p style="font-family: Tahoma;"><span>BG-5, Mid City Center, Express-way,</span> Islamabad</p>
      </div>

      <div>
        <i class="fa fa-phone"></i>
        <p style="font-family: Tahoma;">+92 51 846 0676-78</p>
      </div>
      <div>
        <i class="fa fa-envelope"></i>
        <p style="font-family: Tahoma;"><a href="#">info@greenageservices.com</a></p>
      </div>
    </div>
    <div class="footer-right" style="font-family: Tahoma;">
      <h5 class="widget-title" style="color: white; font-family: Tahoma;" >STAY CONNECTED</h5>
      <div class="footer-icons" style="font-family: Tahoma;">
        <a href="https://www.facebook.com/greenageservice"><i class="fa fa-facebook"></i></a>
        <a href="https://twitter.com/GreenAgeService"><i class="fa fa-twitter"></i></a>
        <!-- <a href="#"><i class="fa fa-instagram"></i></a> -->
        <a href="https://www.linkedin.com/in/greenageservices/"><i class="fa fa-linkedin"></i></a>
        <a href="https://www.youtube.com/watch?v=5jVGwuTwTbA"><i class="fa fa-youtube"></i></a>
      </div>
    </div>
  </footer>
  <!-- ##### Footer Area End ##### -->

  <!-- ##### All Javascript Files ##### -->
  <!-- jquery 2.2.4  -->
  <script src="js/jquery.min.js"></script>
  <!-- Popper js -->
  <script src="js/popper.min.js"></script>
  <!-- Bootstrap js -->
  <script src="js/bootstrap.min.js"></script>
  <!-- Owl Carousel js -->
  <script src="js/owl.carousel.min.js"></script>
  <!-- Classynav -->
  <script src="js/classynav.js"></script>
  <!-- Wow js -->
  <script src="js/wow.min.js"></script>
  <!-- Sticky js -->
  <script src="js/jquery.sticky.js"></script>
  <!-- Magnific Popup js -->
  <script src="js/jquery.magnific-popup.min.js"></script>
  <!-- Scrollup js -->
  <script src="js/jquery.scrollup.min.js"></script>
  <!-- Jarallax js -->
  <script src="js/jarallax.min.js"></script>
  <!-- Jarallax Video js -->
  <script src="js/jarallax-video.min.js"></script>
  <!-- Active js -->
  <script src="js/active.js"></script>
</body>

</html>';