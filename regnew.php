<html>
<head>
       <title> User Registration Form </title>
	   <link rel="stylesheet" type ="text/css" href="style.css">
	   <meta name="viewport" content="width=device-width, initial-scale=1">
	   <link rel="stylesheet" type ="text/css"
	   href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	 
<style>

	 .container {
            border-radius: 5px;
            background: white;
            padding: 20px;
            width: 40%;
            margin: auto;
            float: center;
            text-align: center;
            margin-top: 2%;
            font: Tahoma;
            background-position: center;
			/* box-sizing: border-box; */
			 
        }
		* {
  box-sizing: border-box;
}
		
</style>


</head>
<body style="color: grey; font: Tahoma;">

<form method ="post" style="font: Tahoma;">

  <div class="container" style="text-align: center; background-position: center; font:Tahoma;">
  
	  <h2 style="color: black;">Registration</h2>
	  <p style="color: black;">Create your account. it's free and only takes a minute.</p>
	 <center>
<table class="table" style="padding: 5%; background-position: center;">
	<tr>
		<td>
			<div class="form-group">
				<label>Firstname<span class="req">*</span></label>
				<input type="text" name="Firstname" class="form-control" placeholder="Firstname" style="border-radius: 5px" required>
				
				</div>
				
		</td>
	
	
		<td>
			<div class="form-group">
				<label>Lastname<span class="req">*</span></label>
				<input type="text" name="Lastname" class="form-control" placeholder="Lastname" style="border-radius: 5px" required>
				
				</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="form-group">
				<label>Email</label>
				<input type="varchar" name="Email" class="form-control" placeholder="Email"  style="border-radius: 5px" required>
				
				</div>
		</td>
	
		<td>
			<div class="form-group">
				<label>Phone No.</label>
				<input type="tel" name="Phone" class="form-control" placeholder="Phone no"  style="border-radius: 5px" required>
				
				</div>
		</td>
	</tr>
	<tr>
		<td>
			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" class="form-control" placeholder="Password"style="border-radius: 5px" required>
				</div>
		</td>
	
		<td>
			<div class="form-group">
				<label>Confirm Password</label>
				<input type="Password" name="ConfirmPassword" class="form-control" placeholder="Confirm Password" style="border-radius: 5px" required>
				</div>
		</td>
    </tr>
    <tr>
		<td>
           
            <div class="form-group" >
				<label>CNIC No.</label>
				<input type="text" name="CNIC" class="form-control" placeholder="35343-5198630-1"  style="border-radius: 5px" required >
				</div>
        </td>
        <td>
        <!-- pattern="[0-9]{5}-[0-9]{7}-[0-9]{1}" -->
            <div class="form-group" >
				<label>Select Category</label></br>
                <select name="usertype" id="usertype" style="padding: 3%; border-radius: 5px">
                    
                <option default value="select">Select</option>
                    <option value="students">Students</option>
                    <option value="Lecturer">Lecturer</option>
                    <option value="Farmer">Farmer</option>
                    <option value="Household">Household</option>
                    
                  </select>
				</div>
        </td>
        </tr>
</table>  </center> 
		  
		  <div class="contact100-form-checkbox" style="margin-top: -8%;">
      <input class="input-checkbox" id="checkbox" type="checkbox" name="I accept the Term of Use & Privacy Policy">
           <label class="label-checkbox100" for="checkbox">
               I accept the Term of Use & Privacy Policy
           </label>
            </div>
		
		<center>
		  <button type="submit" class="btn btn-primary" name="submit"  onclick="myFunction()">Register</button></center>
		 
		  
		  </form>
	
</div>
<script>
function myFunction() {
  alert("Registered Successfully!");
  
}
	</script>
</body>
</html>

<?php 

include "db_config.php";
$dist_array = array();

if(isset($_POST['submit'])){
    
    $Firstname = $_POST['Firstname'];
    $Lastname= $_POST['Lastname'];
    $Email= $_POST['Email'];
    $Phone= $_POST['Phone'];
    $password= $_POST['password'];
    $cnic= $_POST['CNIC'];
    $usertype= $_POST['usertype'];
    
    if($usertype=='students'){
        $userTypeId = "1";
    } else if($usertype=='Lecturer'){
        $userTypeId = "2";
    } else if($usertype=='Farmer'){
        $userTypeId = "3";
    } else if($usertype=='Household'){
        $userTypeId = "4";
    }
    
    $now = new DateTime();
    echo $now->format('d-m-Y');
    
    $fullname = $Firstname.' '.$Lastname;
    echo $password;
    
    $province_query = mysqli_query($conn,"select * from province where id = '".$cnic[0]."' ") or die(mysqli_error($conn));
    $province_res = mysqli_fetch_array($province_query);
    $province_id = $province_res['id'];               // province id
    $province = $province_res['province'];
    echo $province;
    
    $div_query = mysqli_query($conn,"select * from divisions where province_id = '".$cnic[0]."' AND id = '".$cnic[1]."'") or die(mysqli_error($conn));
    $div_res = mysqli_fetch_array($div_query);
    $div_id = $div_res['id'];               // division id
    $division = $div_res['division'];
    echo $division;
    
    $dist_query = mysqli_query($conn,"select * from districts where province_id = '".$cnic[0]."' AND division_id = '".$cnic[1]."'") or die(mysqli_error($conn));
    $row_count = mysqli_num_rows($dist_query);

    if($row_count > 0){
		while($row = mysqli_fetch_array($dist_query)){
	        array_push($dist_array,$row[1]);
        }
        echo $dist_array[$cnic[2]];
        $district = $dist_array[$cnic[2]];
    }
    
    $result = mysqli_query($conn,"INSERT INTO `farmers`(`fullname`,
														`phone`, 
														`cnic`, 
														`province`, 
														`division`, 
														`district`,
														`status`,
														`image`, 
														`email`,
														`password`,
														`type`) 
														VALUES ('$fullname',
														'$Phone',
														'$cnic',
														'$province',
														'$division',
														'$district',
														'1',
														'pic',
														'$Email',
														'$password',
														'$userTypeId')
													") or die(mysqli_error());
													
													if($result>0){
													    echo alert("You have been registered!");
													}else{
													    echo "failed";
													}
    

    
}else {
    echo "no cnic";
}

if(isset($_POST['submit'])){

    header('Location: regnew.php');
}

?>