<!DOCTYPE html>
<html lang="en">

<head>
  <link rel="stylesheet" href="css/gallery.css" />
  <meta charset="UTF-8">
  <meta name="description" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <!-- Title -->
  <title>Greenage - Provide Services | Plants</title>
  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="180x180" href="img/core-img/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/core-img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/core-img/favicon-16x16.png">
  <link rel="manifest" href="img/core-img/site.webmanifest">
  <!-- Core Stylesheet -->
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="css/gallery.css" />

  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="footer, address, phone, icons" />


	<link rel="stylesheet" href="style.css">
	
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="http://fonts.googleapis.com/css?family=Cookie" rel="stylesheet" type="text/css">
  

  <style>
div.myGallery {
            /* border: 2px solid orange; */
            border-radius: 8px;
         }
         div.myGallery:hover {
            border: 1px solid blue;
            border-radius: 8px;
         }
         div.myGallery img {
            width: 100%;
            height: auto;
            border-radius: 8px;
         }
         div.desc {
            padding: 20px;
            text-align: center;
         }
         .responsive {
            padding: 0 5px;
            float: left;
            width: 24.99999%;
            margin-bottom: 12px;
            animation-name: zoom;;
         }
         @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }
  
  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }

         @media only screen and (max-width: 700px) {
            .responsive {
               width: 49.99999%;
               margin: 5px 0;
            }
         }
         @media only screen and (max-width: 500px) {
            .responsive {
               width: 100%;
            }
         }
         .clearfix:after {
            content: "";
            display: table;
            clear: both;
         }

 @import url('http://fonts.googleapis.com/css?family=Open+Tahoma:400,700');

*{
    padding:0;
    margin:0;
}

html{
    background-color: #eaf0f2;
}

body{
    font:16px/1.6 Tahoma;
}


footer{
    position: fixed;
    bottom: 0;
}

@media (max-height:800px){
    footer { position: static; }
    /* header { padding-top:40px; } */
}


.footer-distributed{
    background-color: #2c292f;
    box-sizing: border-box;
    width: 100%;
    text-align: left;
    font: bold 16px sans-serif;
    padding: 50px 50px 60px 50px;
    margin-top: 80px;
}

.footer-distributed .footer-left,
.footer-distributed .footer-center,
.footer-distributed .footer-right{
    display: inline-block;
    vertical-align: top;
}

/* Footer left */

.footer-distributed .footer-left{
    width: 30%;
}

.footer-distributed h3{
    color:  #ffffff;
    font: normal 36px 'Cookie', cursive;
    margin: 0;
}

/* The company logo */

.footer-distributed .footer-left img{
    width: 30%;
}

.footer-distributed h3 span{
    color:  #8cdb31;
}

/* Footer links */

.footer-distributed .footer-links{
    color:  #ffffff;
    margin: 20px 0 12px;
}

.footer-distributed .footer-links a{
    display:inline-block;
    line-height: 1.8;
    text-decoration: none;
    color:  inherit;
}

.footer-distributed .footer-company-name{
    color:  #f7f7f7;
    font-size: 14px;
    font-weight: normal;
    margin: 0;
    
}

/* Footer Center */

.footer-distributed .footer-center{
    width: 35%;
}


.footer-distributed .footer-center i{
    background-color:  #33383b;
    color: #ffffff;
    font-size: 25px;
    width: 38px;
    height: 38px;
    border-radius: 50%;
    text-align: center;
    line-height: 42px;
    margin: 10px 15px;
    vertical-align: middle;
}

.footer-distributed .footer-center i.fa-envelope{
    font-size: 17px;
    line-height: 38px;
}

.footer-distributed .footer-center p{
    display: inline-block;
    color: #ffffff;
    vertical-align: middle;
    margin:0;
}

.footer-distributed .footer-center p span{
    display:block;
    font-weight: normal;
    font-size:14px;
    line-height:2;
}

.footer-distributed .footer-center p a{
    color:  #ffffff;
    text-decoration: none;;
}


/* Footer Right */

.footer-distributed .footer-right{
    width: 30%;
}

.footer-distributed .footer-company-about{
    line-height: 20px;
    color:  #92999f;
    font-size: 13px;
    font-weight: normal;
    margin: 0;
}

.footer-distributed .footer-company-about span{
    display: block;
    color:  #ffffff;
    font-size: 18px;
    font-weight: bold;
    margin-bottom: 20px;
}

.footer-distributed .footer-icons{
    margin-top: 25px;
}

.footer-distributed .footer-icons a{
    display: inline-block;
    width: 35px;
    height: 35px;
    cursor: pointer;
    background-color:  #33383b;
    border-radius: 2px;

    font-size: 20px;
    color: #ffffff;
    text-align: center;
    line-height: 35px;

    margin-right: 3px;
    margin-bottom: 5px;
}

/* Here is the code for Responsive Footer */
/* You can remove below code if you don't want Footer to be responsive */


@media (max-width: 880px) {

    .footer-distributed .footer-left,
    .footer-distributed .footer-center,
    .footer-distributed .footer-right{
        display: block;
        width: 100%;
        margin-bottom: 40px;
        text-align: center;
    }

    .footer-distributed .footer-center i{
        margin-left: 0;
    }

}




    #myImg {
    border-radius: 5px;
    cursor: pointer;
    transition: 0.3s;
  }
  
  #myImg:hover {opacity: 0.7;}
  
  /* The Modal (background) */
  .modal {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 100px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
  }
  
  /* Modal Content (image) */
  .modal-content {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
  }
  
  /* Caption of Modal Image */
  #caption {
    margin: auto;
    display: block;
    width: 80%;
    max-width: 700px;
    text-align: center;
    color: #ccc;
    padding: 10px 0;
    height: 150px;
  }
  
  /* Add Animation */
  .modal-content, #caption {  
    -webkit-animation-name: zoom;
    -webkit-animation-duration: 0.6s;
    animation-name: zoom;
    animation-duration: 0.6s;
  }
  
  @-webkit-keyframes zoom {
    from {-webkit-transform:scale(0)} 
    to {-webkit-transform:scale(1)}
  }
  
  @keyframes zoom {
    from {transform:scale(0)} 
    to {transform:scale(1)}
  }
  
  /* The Close Button */
  .close {
    /* position: absolute; */
    top: 15px;
    right: 35px;
    color: white;
    font-size: 40px;
    font-weight: bold;
    transition: 0.3s;
    /* size: 40px; */
  }
  
  .close:hover,
  .close:focus {
    color: white;
    text-decoration: none;
    cursor: pointer;
  }
  
  /* 100% Image Width on Smaller Screens */
  @media only screen and (max-width: 700px){
    .modal-content {
      width: 100%;
    }
  }

  /* Dropdown Gallery */

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 16px;  
  border: none;
  outline: none;
  color: white;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;
}

/* .navbar a:hover, .dropdown:hover .dropbtn {
  background-color: red;
} */

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #fffdfd;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0/.2);
  z-index: 1;
  color: black;
}

.dropdown-content a {
  float: none;
  color: white;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {
  background-color:rgb(143, 148, 143);
}
  
  </style>


</head>

<body>
  <!-- Preloader -->
  <div class="preloader d-flex align-items-center justify-content-center">
    <div class="spinner"></div>
  </div>

  <!-- ##### Header Area Start ##### -->
  <header class="header-area">
    <!-- Top Header Area -->
    <div class="top-header-area">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="top-header-content d-flex align-items-center justify-content-between">
              <!-- Top Header Content -->
              <div class="top-header-meta">
                <p>Welcome to <span>Greenage</span>, we hope you will enjoy our services and have good experience</p>
              </div>
              <!-- Top Header Content -->
              <div class="top-header-meta text-right">
                <a href="contact.php" data-toggle="tooltip" data-placement="bottom" title="info@greenageservices.com"><i
                    class="fa fa-envelope-o" aria-hidden="true"></i> <span>Email: info@greenageservices.com</span></a>
                <a href="contact.php" data-toggle="tooltip" data-placement="bottom" title="+92 51 846 0676"><i class="fa fa-phone"
                    aria-hidden="true"></i> <span>Call Us: +92 51 846 0676-78</span></a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Navbar Area -->
    <div class="famie-main-menu" style="text-align: left; margin-left: 0%; margin-right: 0%;">
      <div class="classy-nav-container breakpoint-off">
        <div class="container">
          <!-- Menu -->
          <nav class="classy-navbar justify-content-between" id="famieNav">
            <!-- Nav Brand -->
            <div>
            <a href="index.php" class="nav-brand" style="margin-right: 0%; margin-left: 0%;">
             
              <img src="img/core-img/website_logo.png" alt="" width="70" height="70"></a>
              <h class="head" style="text-align: right; color: #77b122; margin-right: 0%; font-family: Tahoma;">GreenAge Services</h>
             </div> 
            <!-- Navbar Toggler -->
            <div class="classy-navbar-toggler">
              <span class="navbarToggler"><span></span><span></span><span></span></span>
            </div>
            <!-- Menu -->
            <div class="classy-menu " style="text-align: right;">
              <!-- Close Button -->
              <div class="classycloseIcon" style="margin-left: 10%; font-family: Tahoma;">
                <div class="cross-wrap"><span class="top" ></span><span class="bottom"></span></div>
              </div>
              <!-- Navbar Start -->
              <div class="classynav" style="text-align: right; font-family: Tahoma;">
                <ul class="nav" style="text-align: right; margin-left: 0%; margin-right: 0%; font-family: Tahoma;">
                  <li style="font-family: Tahoma;"><a href="index.php">Home</a></li>
                  <li style="font-family: Tahoma;"><a href="about.php">About</a></li>
                  <!-- <li><a href="#">Pages</a>
                    <ul class="dropdown">
                      <li><a href="index.html">Home</a></li>
                      <li><a href="about.html">About Us</a></li> -->
                      <!-- <li><a href="farming-practice.html">Farming Practice</a></li> -->
                      <!-- <li><a href="shop.html">Shop</a>
                        <ul class="dropdown">
                          <li><a href="our-product.html">Our Services</a></li> -->
                          <!-- <li><a href="shop.html">Shop</a></li> -->
                        <!-- </ul> -->
                      <!-- </li> -->
                      <!-- <li><a href="news.html">News and Events</a>
                        <ul class="dropdown"> -->
                          <!-- <li><a href="news.html">News and Events</a></li> -->
                          <!-- <li><a href="news-details.html">News Details</a></li> -->
                        <!-- </ul> -->
                      <!-- </li> -->
                      <!-- <li><a href="contact.html">Contact</a></li> -->
                    <!-- </ul> -->
                  <!-- </li> -->
                  <!-- <li><a href="our-product.html">Our Product</a></li> -->
                  <!-- <li><a href="farming-practice.html">Farming Practice</a></li> -->
                    <!-- <li><a href="#">Market Place</a></li> -->
                    <li ><a href="mandirates.php">Mandi Rates</a></li>
                    <!-- <li><a href="#">Crop Simulator</a></li> -->
                    <li><a href="contact.php">Contact</a></li>
                    <li  class="active"><a href="gallery.php">Gallery</a>
                      <ul class="dropdown">
                        <!-- <button class="dropbtn">Gallery 
                          <i class="fa fa-caret-down"></i>
                        </button> -->
                        <!-- <div class="dropdown-content"> -->
                         <li class="dropdown-content"><a href="fruit.php">Fruits</a></li> 
                         <li class="dropdown-content"><a href="veg.php">vegetables</a></li>  
                         <li class="dropdown-content"> <a href="plant.php">Plants</a></li> 
                        <!-- </div> -->
                        </ul>
                    </li>
                    <!-- <li><a href="#">Help &amp; Support</a></li> -->
                    <!-- <li><a href="#">News &amp; Events</a></li> -->
                  </ul>
                   <!-- Search Icon -->
                <div id="searchIcon">
                  <i class="icon_search" aria-hidden="true"></i>
                </div>
                  
                
                <!-- Search Icon -->
                <!-- <div id="searchIcon">
                  <i class="icon_search" aria-hidden="true"></i>
                </div> -->
                <!-- Cart Icon -->
                <!-- <div id="cartIcon">
                  <a href="#">
                    <i class="icon_cart_alt" aria-hidden="true"></i>
                    <span class="cart-quantity">2</span>
                  </a>
                </div> -->
              </div>
              <!-- Navbar End -->
            </div>
          </nav>

          <!-- Search Form -->
          <div class="search-form">
            <form action="#" method="get">
              <input type="search" name="search" id="search" placeholder="Type keywords &amp; press enter...">
              <button type="submit" class="d-none"></button>
            </form>
            <!-- Close Icon -->
            <div class="closeIcon"><i class="fa fa-times" aria-hidden="true"></i></div>
          </div>
        </div>
      </div>
    </div>
  </header>
  <!-- ##### Header Area End ##### -->

  <!-- ##### Breadcrumb Area Start ##### -->
  <div class="container">
  <div class="breadcrumb-area bg-img bg-overlay jarallax" style="background-image: url('img/bg-img/plant.jpg');">
    <div class="container h-100">
      <div class="row h-100 align-items-center">
        <div class="col-12">
          <div class="breadcrumb-text">
            <h2>Ornamental Plants</h2>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

  <div class="famie-breadcrumb">
    <div class="container">
      <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.html"><i class="fa fa-home"></i>Home</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="gallery.php">Gallery</a></li>
          <li class="breadcrumb-item active" aria-current="page"><a href="plant.php">Plants</a></li>
          
        </ol>
      </nav>
    </div>
  </div>
			<div class="tm-paging-links" style="margin-left: 35%;">
				<nav>
          <!-- <ul>
					<a href="agro.html" class="btn famie-btn mt-30" style="margin-top:auto; margin-right: 1%;" onclick="javascript:ShowHide('HiddenDiv')">Agronomic</a>
					<a href="veg.html" class="btn famie-btn mt-30" style="margin-top:auto; margin-right: 1%;" onclick="javascript:ShowHide('HiddenDiv')">Vegetables</a>
					<a href="fruit.html" class="btn famie-btn mt-30" style="margin-top:auto;" onclick="javascript:ShowHide('HiddenDiv')">Fruits</a> -->
					</ul>
				</nav>
			</div>
			
<p class="para" style="text-align: center; margin-left: 15%; margin-right: 15%; margin-top: 1%;">
    Here you will get plants.
</p>
    <p class="para" style="text-align: center; margin-left: 15%; margin-right: 15%; margin-top: 1%;">
        Ornamental plants are plants that are grown for decorative purposes in 
        gardens and landscape design projects, as houseplants, cut flowers and specimen display.
     </p>
	<img src="img/core-img/decor.png" class="img" style="margin-left: 45.7%; margin-right: 15%; margin-top: 0%;" alt="">

  <!-- Gallery -->
  <div class="row tm-gallery" style="margin-top: 3%; margin-left: 12%; text-align: center; margin-right: 12%; flex: 20%; content: width=device-width;">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

				
    <div class = "responsive">
      <div class = "myGallery">
         <a target = "_blank" href="img/plant/1.jpg">
            <img src = "img/plant/1.jpg" alt="corn" width="600" height="500">Show more
         </a>
         <div class = "mydiv">French Lavender</div>
      </div>
   </div>
   <div class = "responsive">
      <div class = "myGallery">
         <a target = "_blank" href = "img/plant/2.jpg">
            <img src = "img/plant/2.jpg" alt="" width="600" height="500">Show more
         </a>
         <div class = "mydiv">Marigold</div>
      </div>
   </div>
   <div class = "responsive">
      <div class = "myGallery">
         <a target = "_blank" href = "img/plant/3.jpg">
            <img src = "img/plant/3.jpg" alt="cotton" width="600" height="500">Show more
         </a>
         <div class="mydiv">Pereskia Aculeata</div>
      </div>
   </div>
   <div class = "responsive">
     <div class = "myGallery">
        <a target = "_blank" href="img/plant/4.jpg">
           <img src = "img/plant/4.jpg" alt="corn" width="600" height="500">Show more
        </a>
        <div class = "mydiv">Night-blooming Jesamine</div>
     </div>
  </div>
  <div class = "responsive">
     <div class = "myGallery">
        <a target = "_blank" href = "img/plant/5.jpg">
           <img src = "img/plant/5.jpg" alt="" width="600" height="500">Show more
        </a>
        <div class = "mydiv">Red Rose</div>
     </div>
  </div>
  <div class = "responsive">
     <div class = "myGallery">
        <a target = "_blank" href = "img/plant/6.jpg">
           <img src = "img/plant/6.jpg" alt="cotton" width="600" height="500">Show more
        </a>
        <div class="mydiv">San Padro Cactus</div>
     </div>
  </div>
  <div class = "responsive">
     <div class = "myGallery">
        <a target = "_blank" href="img/plant/7.jpg">
           <img src = "img/plant/7.jpg" alt="corn" width="600" height="500">Show more
        </a>
        <div class = "mydiv">Schlumber Truncata</div>
     </div>
  </div>
  <div class = "responsive">
     <div class = "myGallery">
        <a target = "_blank" href = "img/plant/8.jpg">
           <img src = "img/plant/8.jpg" alt="" width="600" height="500">Show more
        </a>
        <div class = "mydiv">Schlumber Gaertneri</div>
     </div>
  </div>
  <div class = "responsive">
     <div class = "myGallery">
        <a target = "_blank" href = "img/plant/9.jpg">
           <img src = "img/plant/9.jpg" alt="cotton" width="600" height="500">Show more
        </a>
        <div class="mydiv" style="text-align: center;">Jasmine</div>
     </div>
  </div>
  
</div>

    <script src="js/jquery.min.js"></script>
    <script src="js/parallax.min.js"></script>
    
</body>


 <!-- ##### Footer Area Start ##### -->
 <footer class="footer-distributed" style="font-family: Tahoma; margin-top: 1%;">

  <div class="footer-left" style="font-family: Tahoma;">
      <img src="img/core-img/website_logo.png" style="width:90; height:90; position: center;">
    <h3 style="font-family: Tahoma;">GreenAge<span>Services</span></h3>

  

    <p class="footer-company-name" style="font-family: Tahoma;"> Copyright © All rights reserved by GreenAge Services </p>
  </div>

  <div class="footer-center" style="font-family: Tahoma;">
    <div>
      <i class="fa fa-map-marker" style="font-family: Tahoma;">
                  </i>
        <p style="font-family: Tahoma;"><span>BG-5, Mid City Center, Express-way,</span> Islamabad</p>
    </div>

    <div>
      <i class="fa fa-phone"></i>
      <p style="font-family: Tahoma;">+92 51 846 0676-78</p>
    </div>
    <div>
      <i class="fa fa-envelope"></i>
      <p style="font-family: Tahoma;"><a href="#">info@greenageservices.com</a></p>
    </div>
  </div>
  <div class="footer-right" style="font-family: Tahoma;">
    <h5 class="widget-title" style="color: white; font-family: Tahoma;" >STAY CONNECTED</h5>
    <div class="footer-icons" style="font-family: Tahoma;">
      <a href="https://www.facebook.com/greenageservice"><i class="fa fa-facebook"></i></a>
      <a href="https://twitter.com/GreenAgeService"><i class="fa fa-twitter"></i></a>
      <!-- <a href="#"><i class="fa fa-instagram"></i></a> -->
      <a href="https://www.linkedin.com/in/greenageservices/"><i class="fa fa-linkedin"></i></a>
      <a href="https://www.youtube.com/watch?v=5jVGwuTwTbA"><i class="fa fa-youtube"></i></a>
    </div>
  </div>
</footer>
<!-- ##### Footer Area End ##### -->

<!-- ##### All Javascript Files ##### -->
<!-- jquery 2.2.4  -->
<script src="js/jquery.min.js"></script>
<!-- Popper js -->
<script src="js/popper.min.js"></script>
<!-- Bootstrap js -->
<script src="js/bootstrap.min.js"></script>
<!-- Owl Carousel js -->
<script src="js/owl.carousel.min.js"></script>
<!-- Classynav -->
<script src="js/classynav.js"></script>
<!-- Wow js -->
<script src="js/wow.min.js"></script>
<!-- Sticky js -->
<script src="js/jquery.sticky.js"></script>
<!-- Magnific Popup js -->
<script src="js/jquery.magnific-popup.min.js"></script>
<!-- Scrollup js -->
<script src="js/jquery.scrollup.min.js"></script>
<!-- Jarallax js -->
<script src="js/jarallax.min.js"></script>
<!-- Jarallax Video js -->
<script src="js/jarallax-video.min.js"></script>
<!-- Active js -->
<script src="js/active.js"></script>

<script>
  // Get the modal
  var modal = document.getElementById("myModal");
  
  // Get the image and insert it inside the modal - use its "alt" text as a caption
  var img = document.getElementById("myImg");
  var modalImg = document.getElementById("img01");
  var captionText = document.getElementById("caption");
  img.onclick = function(){
    modal.style.display = "block";
    modalImg.src = this.src;
    captionText.innerHTML = this.alt;
  }
  
  // Get the <span> element that closes the modal
  var span = document.getElementsByClassName("close")[0];
  
  // When the user clicks on <span> (x), close the modal
  span.onclick = function() { 
    modal.style.display = "none";
  }
  </script>

</body>

</html>