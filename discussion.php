<!DOCTYPE html>
<html>


<head>


    <title>Select Team Members</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0, user-scalable=yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="apple-touch-icon" sizes="180x180" href="img/core-img/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="img/core-img/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="img/core-img/favicon-16x16.png">
  <link rel="manifest" href="img/core-img/site.webmanifest">

    
    <style type="text/css">body{display:none;}
    </style>

<style type="text/css">body{display:block !important;}
</style>

    <link crossorigin="anonymous" href="https://acctcdn.msauth.net/converged_ux_v2_yQBDHQcAqS8iDHRzxz-bBw2.css?v=1" rel="stylesheet" 
    onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="">

        <script crossorigin="anonymous" src="https://acctcdn.msauth.net/jquerypackage_1.10_5V7LAuc3bNAQx2QQfr1RPw2.js?v=1" 
        onerror="$Loader.On(this,true)" onload="$Loader.On(this)" integrity="sha384-Nltvq0ah1Z9B+Fd7K/cNB9S5RCteYBxHmd5AdBjYCV7R8XVWaTgq5kPRR2GYFGK+">
    </script>
<style>
.sidebar {
  margin-left: 28%;
  margin-top: 12.5%;
  padding: 0;
  width: 200px;
  background-color: #f1f1f1;
  position: fixed;
  height: 45%;
  overflow: auto;
}


.sidebar a {
  display: block;
  color: black;
  padding: 16px;
  text-decoration: none;
}
 
.sidebar a.active {
  background-color: #4CAF50;
  color: white;
}

.sidebar a:hover {
  background-color:grey;
  color: white;
}



@media screen and (max-width: 700px) {
  .sidebar {
    width: 100%;
    height: 100%;
    position: relative;
  }
  .sidebar a {float: left;}
  div.content {margin-left: 0;}
}

@media screen and (max-width: 400px) {
  .sidebar a {
    text-align: center;
    float: none;
  }
}
</style>


    

</head>


<body style="" class="ltr  Chrome Win M81 _D0 Full Win81 RE_WebKit" lang="en-US">
    <div class="App" id="iPageElt">

    <div id="c_base" class="c_base">
    <div id="c_content">
<!--background -->
<div class="background " role="presentation">

<div id="cnvCtrlBgImg" class="backgroundImage" 
style="background-image: url(&quot;https://acctcdn.msauth.net/images/2_vD0yppaJX3jBnfbHF1hqXQ2.svg&quot;);">
</div>
</div>

<div class="sidebar" style="float: left;">
  <a class="active" href="#">Group Members</a>
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Ali</a>
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Ahmed</a>
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Adal</a>
 
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Ali</a>
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Ahmed</a>
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Adal</a>
 
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Ali</a>
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Ahmed</a>
  <a href="#"><img src="img/card.jpg" alt="" style="width:30px; border-radius: 50%;">Adal</a>
</div>
<!--lightbox -->
<div class="outer" role="presentation" style="margin-top: 0%;">

<div class="middle ">
    
    <!-- header -->
    
    <div id="inner" class="inner fade-in-lightbox">
        
        <div class="win-scroll"><center>
            <img src="img/core-img/website_logo.png" class="logo" role="img" alt="GreenAge Services" style="padding-right: 1%; width: 20%; height: 20%;">
            <!-- <b>GreenAge Services</b> -->
        </center>
            <div id="pageContent" class="pagination-view">
                
        
        <div role="main" id="maincontent">
            
<div id="identityBanner" data-bind="component:
    {
        name: 'convergedBanner',
        params: {
            memberName: memberName,
            isBackButtonVisible: isBackButtonVisible,
            strings: strings,
            arrowImg: arrowImg,
            showIdentityBanner: showIdentityBanner
        }
    }">
    <div class="identityBanner" data-bind="if: memberName &amp;&amp; showIdentityBanner, visible: memberName &amp;&amp; showIdentityBanner" 
    style="display: none;">
</div>
</div>

<!-- HIP content -->
<div class="hide" id="hipScript"><script type="text/javascript"></script></div>
<div class="hide" id="hipContent"></div>
<div class="hide" id="hipTemplate"></div>

<div id="pageControlHost">
<div class="pagination-view" style="opacity: 1;"><div id="Credentials">
    <form id="CredentialsForm" action="#">
        <div data-bind="visible: showCredentialsView">
            <div id="CredentialsInputPane" data-bind="visible: !showSuggestions()">
                <div id="CredentialsPageTitle" class="row text-title" role="heading" aria-level="1" data-bind="text: strings.pageTitle">
                   <center> Title</center>

                   
                </div>
                <center>
                 <div class="tex"> 
                <input type="text" placeholder="discription" name="name" class="field" style="border-radius: 5px" required> <br>
         </div>
       
       
         <label style="text-align:left; text-decoration:black;" ><b> Comment:</b></label><br />
  <textarea name='comment' id='comment'></textarea><br />
 
  <input type='hidden' name='articleid' id='articleid' value='<? echo $_GET["id"]; ?>' />
 
  <div class="col-md-4" style="margin-top:2%; color:white; position:relative; border-radius: 8px; float:right;">
  <input type='submit' value='Submit'style="margin-top:2%; background-color:green; color:white; position:relative; border-radius: 8px; float:right;" /> 
  <br></div>
  <br>
  <div class="row">
  <div class="col-md-4" style="margin-top: 5%;  color:white; position:relative; border-radius: 8px;  ">
        <button type="button" href="#" class="btn" style="margin-top: 5%; background-color:green; color:white; position:relative; border-radius: 8px; ">Accept</button> 
   <button type="button" href="#" class="btn" style="margin-top: 5%; background-color:green; color:white; position:relative; border-radius: 8px; ">Reject</button>
    <button type="button" href="#" class="btn" style="margin-top: 5%; background-color:green; color:white; position:relative; border-radius: 8px; ">Review</button>
</div>
  </div>
  <!-- </div> -->
  
                 <!-- <div class="tex" style="margin-top: 3%;"> 
                 <input class="input-checkbox" id="checkbox" type="checkbox" name="name">
         <label class="label-checkbox100" for="checkbox">
                 <input type="text" placeholder="Enter Name" name="name" class="field" style="border-radius: 5px" required></label><br>
                 <input class="input-checkbox" id="checkbox" type="checkbox" name="name">
         <label class="label-checkbox100" for="checkbox">
              <input type="text" placeholder="Enter Email" name="email" class="field" style="border-radius: 5px" required></label><br>
              <input class="input-checkbox" id="checkbox" type="checkbox" name="name">
         <label class="label-checkbox100" for="checkbox">
             <input type="text" placeholder="Enter Discipline" name="discipline" class="field" style="border-radius: 5px" required></label><br>
                 </div>
                 
                 <div class="tex" style="margin-top: 3%;"> 
                 <input class="input-checkbox" id="checkbox" type="checkbox" name="name">
         <label class="label-checkbox100" for="checkbox">
                 <input type="text" placeholder="Enter Name" name="name" class="field" style="border-radius: 5px" required></label><br>
                 <input class="input-checkbox" id="checkbox" type="checkbox" name="name">
         <label class="label-checkbox100" for="checkbox">
              <input type="text" placeholder="Enter Email" name="email" class="field" style="border-radius: 5px" required></label><br>
              <input class="input-checkbox" id="checkbox" type="checkbox" name="name">
         <label class="label-checkbox100" for="checkbox">
              <input type="text" placeholder="Enter Discipline" name="discipline" class="field" style="border-radius: 5px" required></label>
                 </div> -->
                <center>
              
             </center>
                    </div>

                           </div>
    </form>
                </div> 
            

    <!-- footer -->
   <!--  
    <div id="footer" class="footer
        default" role="contentinfo">
        <div class="footerNode text-secondary">
            <a target="_blank" rel="noreferrer noopener" id="terms" href="#">Terms of Use</a>
             <a target="_blank" rel="noreferrer noopener" id="privacy" href="#">Privacy &amp; Cookies</a>
            
        </div>
    </div> -->
    
</div>
</div>
      
    </div>
</div></div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>var jQueryScript = document.createElement('script');  
jQueryScript.setAttribute('src','https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js');
document.head.appendChild(jQueryScript);</script>
<script>
function OnNext() {
    // echo "hekki";exit();
  href("#");
  
}
  </script>
</body>
</html>